Neue Saison
===========

.. image:: /_static/icon-64.*
	:alt: Logo - eine gelbe und eine rote Karte - in grün
	:align: right

.. index:: Neue Saison, Saisonstart, RefereeManager

Zu aktualisierende Dateien
--------------------------

- Downloads neu verlinken, Hilfsskripts:
	- :file:`findbrokenlinks.sh`
	- :file:`finddownloadslastyear.sh`

Mit :program:`RefereeManager` zu erzeugende Dateien
---------------------------------------------------

- Ligen
	- Template: :file:`tt-schiri.de/leagues.json`
	- Ausgabepfad: :file:`tt-schiri.de/lektor/databags/`
	- Ausgabedatei: :file:`leagues.json`
	- Kommunikationsart: Text (1 unabhängig von Auswahl)
- Ortsübersicht
	- Template: :file:`tt-schiri.de/venues_overview.lr`
	- Ausgabepfad: :file:`tt-schiri.de/lektor/content/termine/venues/`
	- Ausgabedatei: :file:`contents.lr`
	- Kommunikationsart: Text (1 unabhängig von Auswahl)
- Ortsdetails
	- Template: :file:`tt-schiri.de/venues_detail.lr`
	- Ausgabepfad: :file:`tt-schiri.de/lektor/content/termine/venues/${current.filename.value?lower_case?replace(".", "_")}/`
	- Ausgabedatei: :file:`contents.lr`
	- Kommunikationsart: Text (1 pro Auswahl)
- VSR-Terminübersicht
	- Template: :file:`tt-schiri.de/events_overview.lr`
	- Ausgabepfad: :file:`tt-schiri.de/lektor/content/termine/events/`
	- Ausgabedatei: :file:`contents.lr`
	- Kommunikationsart: Text (1 unabhängig von Auswahl)
