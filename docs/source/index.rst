tt-schiri.de - Entwicklerdoku
=============================

.. image:: /_static/icon-64.*
	:alt: Logo - eine gelbe und eine rote Karte - in grün
	:align: right

.. highlights::
	Webseiten der Schiedsrichterinnen und Schiedsrichter des Berliner Tisch-Tennis Verbands.

.. toctree::
	:maxdepth: 2
	:caption: Inhalte:

	newseason

tt-schiri.de sind die Webseiten der Schiedsrichterinnen und Schiedsrichter des Berliner Tisch-Tennis Verbands, die hier zu finden sind:

https://www.tt-schiri.de/

Die Webseiten werden mit :program:`lektor` erzeugt, dieses Repository enthält alle Quellen dafür.
