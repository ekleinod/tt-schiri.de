# tt-schiri

Dieses Repository enthält die Webseite der Schiedsrichter des Berliner Tisch-Tennis Verbands (BTTV):

- https://www.tt-schiri.de/

Die Webseite wird mit Lektor erstellt, im Repository sind sowohl die Quellen als auch die generierten Webseiten enthalten.

Die Seiten nutzen SymLinks, um Dateien nicht doppelt zu halten und die Webseiten etwas unabhängiger von den konkreten Informationen.
Das heißt, die Übersetzung ist nur auf einem Rechner möglich, auf dem die Ziele aller SymLinks genau so vorhanden sind.

[Zur Dokumentation](https://tt-schiride.readthedocs.io/de/latest/)

## Autoren

- Ekkart Kleinod
	- gitlab: [ekleinod](https://gitlab.com/ekleinod)
	- E-Mail: <schiri@ekkart.de>
- Tobias Kantusch
	- github: [tkantusch](https://github.com/tkantusch)
	- E-Mail: <tobiaskantusch@online.de>

## Mitmachen

Wer Fehler in den Webseiten findet, etwas verbessern will oder mitmachen möchte, hat vier Möglichkeiten:

1. Mail an einen Autor
2. Issue eröffnen: https://gitlab.com/ekleinod/tt-schiri.de/issues
3. forken und pull request erstellen
4. direkt als Autor eingetragen werden

## Git-Repository

Die Webseite wird grundsätzlich über den master-Branch aktuell gehalten.
Programmierung wird über feature-Branches nach dem "stable mainline model" (http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/) organisiert.

Das heißt, der `master`-Branch enthält immer eine funktionierende Version.

## Rechtliches

Copyright 2016-2020 Ekkart Kleinod <schiri@ekkart>

Die Webseiten und jeglicher dazugehöriger Code bzw. Icons stehen, sofern sie selbst erstellt wurden, unter der Lizenz:

Creative-Commons-Lizenz Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International

http://creativecommons.org/licenses/by-nc-sa/4.0/deed.de

Laienhaft zusammengefasst: die selbsterstellten Dinge können verwendet werden, wenn die Autoren genannt werden und die Weitergabe ebenfalls unter gleicher Lizenz erfolgt.
Die Nutzung darf nicht kommerziell sein, ist eine kommerzielle Nutzung erwünscht, müssen die Autoren gefragt werden, die entscheiden ob und unter welchen Bedingungen eine kommerzielle Nutzung erlaubt ist.
