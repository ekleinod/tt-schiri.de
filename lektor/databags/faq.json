{
  "FAQ1": {
    "question": "Im Doppel verletzt sich ein Spieler so, dass er seine Einzel nicht bestreiten kann.\n*Muss* in der Einzelaufstellung aufgerückt werden?",
    "answer": "Es kann aufgerückt werden, da die endgültige Einzelaufstellung erst nach den Anfangsdoppeln erfolgen muss.\nEs braucht nicht aufgerückt zu werden, da der Spieler im Mannschaftskampf mitgewirkt hat und nicht als \"ausgefallen\" gilt.",
    "see": "- [WO des BTTV](/downloads/ordnungen/BeTTV-Wettspielordnung.pdf), D.3\n- [WO des DTTB](/downloads/ordnungen/DTTB-Wettspielordnung.pdf), D.3",
    "date": "2016-12-17"
  },
  "FAQ2": {
    "question": "Kann eine Einzelaufstellung noch abgeändert werden, wenn das erste Einzel bereits begonnen hat, während ein Doppel noch läuft, und wann beginnt in diesem Sinne ein Einzel?",
    "answer": "Eine Mannschaft, die es zulässt, dass das erste Einzel beginnt, während noch ein Doppel läuft, verzichtet damit auf das ihr an sich zustehende Recht einer Aufstellungsänderung.\nEin Spiel beginnt im Sinne dieser Bestimmung mit dem ersten Aufschlag.",
    "date": "2016-12-17"
  },
  "FAQ3": {
    "question": "Ein Ball geht im Ballwechsel kaputt - gibt es Wiederholung oder Punkt?",
    "answer": "Seit der Einführung der Plastikbälle nicht selten - Spielerin A spielt den Ball, er fliegt über den Tisch hinaus.\nBeim Aufheben stellt Spielerin B fest, dass der Ball kaputt ist.\nWiederholung oder Punkt für B?\n\nHier hat sich die Regelauslegung 2017 geändert.\nEs gilt\n- bemerkt der Schiri den defekten Ball während des Ballwechsels, gibt es immer Wiederholung\n\n- wird der defekte Ball erst nach dem Ballwchsel festgestellt, gibt es nur Wiederholung, wenn beide Spielerinnen das fordern, ansonsten wird auf Punkt entschieden\n\nIm Beispiel wäre also Punkt für Spielerin B die richtige Antwort.",
    "date": "2017-12-01"
  },
  "FAQ4": {
    "question": "Eine Spielerin ist verletzt, wenn sie nicht mitspielt gibt es Strafe und die anderen müssen aufrücken, Ersatz ist nicht in Sicht. Wie kann man nun korrekt aufstellen?",
    "answer": "Die Wettspielordnung sagt:\n\n> Ein Spieler hat an einem Mannschaftskampf mitgewirkt, wenn er zu mindestens einem Einzel oder Doppel antritt und dieses auch in die Wertung eingeht.\nEine Mitwirkung ist schon dann gegeben, wenn der aufgestellte Spieler bei der Begrüßung anwesend ist.\n\nDas heißt, entweder ist die Spielerin bei der Begrüßung anwesend und geht dann wieder (oder schaut zu). Oder sie kommt zu ihren Doppel oder Einzel, ein Aufschlag wird gespielt und sie gibt das Spiel auf.\n\nWichtig: im zweiten Fall muss das Einzel bzw. Doppel in die Wertung eingehen, also mindestens begonnen werden.",
    "see": "- [WO des BTTV](/downloads/ordnungen/BeTTV-Wettspielordnung.pdf), E.4.1",
    "date": "2018-01-01"
  },
  "FAQ5": {
    "question": "Man muss ein Spiel aufgeben, kann nicht antreten oder ist nur zur Aufstellung anwesend - was bedeutet das für die LivePZ bzw. den TTR-Wert?",
    "answer": "Vorausgeschickt sei: die folgenden Ausführungen gelten für Mannschaftsspiele in Mannschaftswettbewerben, nicht für Turniere oder Pokal.\n\nDie Wettspielordnung des DTTB regelt das für TTR-Werte, diese Regelungen gelten analog für die LivePZ-Werte des Berliner Tischtennisverbands.\nFür Einzelspiele werden vier Fälle unterschieden, dazu wird festgelegt, ob die Spiele bei der LPZ-Berechnung berücksichtigt werden:\n\n1. Einzel, bei denen ein Spieler aufgegeben hat: werden berücksichtigt.\n2. Einzel, bei denen ein Spieler auf das Spiel verzichtet hat: werden berücksichtigt; der Spieler muss im Spielberichtsbogen eingetragen worden sein\n3. Einzel, bei denen ein Spieler nicht angetreten ist: werden nicht berücksichtigt; der Spieler darf nicht benannt worden sein, also nicht im Spielberichtsbogen eingetragen sein\n4. Einzel, die wegen Regelverstoßes umgewertet worden sind: werden wie gewertet berücksichtigt\n\nDer Vollständigkeit halber seien noch die Problemfälle für Spieler benannt, die aus Wertungen für Mannschaften resultieren:\n1. Einzel aus Mannschaftskämpfen zurückgezogener Mannschaften: werden berücksichtigt\n2. Einzel aus Mannschaftskämpfen gestrichener Mannschaften: werden berücksichtigt\n3. Einzel aus wegen Nichtantretens kampflos gewerteten Mannschaftskämpfen: werden nicht berücksichtigt\n4. Einzel aus wegen Regelverstoßes umgewerteten Mannschaftskämpfen: werden wie gespielt berücksichtigt",
    "see": "- [WO des BTTV](/downloads/ordnungen/BeTTV-Wettspielordnung.pdf), E.3.1",
    "date": "2018-02-01"
  },
  "FAQ6": {
    "question": "Wir kennen das - die Zeit drängt, das Punktspiel zieht sich, dürfte eigentlich an drei Tischen weitergespielt werden? Muss an zwei Tischen begonnen werden?",
    "answer": "Hier gab es in dieser Saison Änderungen, da der DTTB die Wettspielordnung für den gesamten Bereich des DTTB, also auch den Berliner Verband geändert hat.\n\nGrundsätzlich gilt (wie bisher): \"Sechser- und Vierer-Mannschaften spielen grundsätzlich an zwei Tischen\".\n\nEs gibt jetzt zwei Ausnahmen, die in Berlin zugelassen sind, also angewendet werden dürfen:\n\n1. \"die Heimmannschaft [darf] die Anzahl der Spieltische ohne Zustimmung der Gastmannschaft um einen erhöhen\"\n2. \"Erhöhungen der Tischanzahl [sind] im Einvernehmen beider Mannschaften zulässig\"\n\nDas heißt, die Heimmannschaft darf immer bestimmen, dass an einem Tisch mehr gespielt wird.\nDie Gastmannschaft durfte das bisher auch fordern, das wurde gestrichen, jetzt müssen sich Heim- und Gastmannschaft einig sein.\n\nEs wird insbesondere nichts dazu gesagt, wann erhöht werden kann, man kann also ein Punktspiel auch an drei Tischen beginnen.",
    "see": "- [WO des BTTV](/downloads/ordnungen/BeTTV-Wettspielordnung.pdf), I.5.8",
    "date": "2018-03-01"
  },
  "FAQ7": {
    "question": "Aus Versehen den Tisch verrutscht – wie geht's weiter?",
    "answer": "Grundsätzlich gilt: Tisch verrutschen ist nicht erlaubt, ob absichtlich oder versehentlich, geschieht das im Ballwechsel, gibt es Punkt für die Gegnerin.\nDann wird der Tisch wieder geradegerückt, das Netz neu ausgemessen und weitergespielt.\n\nBei myTischtennis lag der Fall etwas anders: \"Der Ball von Spieler A hat gerade die Grundlinie Richtung Aus überquert, als Spieler B aus Versehen den Tisch verrutscht.\"\n\nEntscheidend ist jetzt: geschieht das Verrutschen des Tischs während des Ballwechsels oder danach?\n\nRegel 10.1.4 besagt: Ein Spieler erzielt einen Punkt, wenn der Ball sein Spielfeld oder seine Grundlinie passiert, ohne sein Spielfeld zu berühren, nachdem er von seinem Gegner geschlagen wurde.\n\nDas heißt, mit dem Passieren des Balls der Grundlinie erzielt Spieler B einen Punkt (mal von dem sehr theoretischen Fall abgesehen, dass der überstarke Unterschnitt oder der Wind den Ball wieder zurück auf das Spielfeld treibt). Ob er danach den Tisch verrutscht ist unerheblich, genauso wie des Aufhalten oder Auffangen des Balls.\n\nDie Regel ist hier eindeutig, es war etwas verwirrend, dass myTischtennis das anfänglich falsch beantwortete, aber Ihr wisst es jetzt besser.",
    "see": "- [TT-Regeln A](/downloads/regeln/Internationale_Tischtennisregeln_A.pdf), 10.1.4",
    "date": "2018-04-01"
  },
  "FAQ8": {
    "question": "Muss sich meine Gegnerin mit mir einspielen?",
    "answer": "Kurz gesagt: nein.\n\nDie Einspielzeit von zwei Minuten ist eine Kann-Bestimmung für die Spielerinnen, das heißt, sie kann genutzt werden, muss aber nicht.\nWenn eine Spielerin nicht will, geht das Spiel sofort los.\nDas heißt insbesondere auch, dass nicht einfach zwei Minuten mit einer Mannschaftskameradin gespielt werden darf.\n\nUnd weil der Text sonst so kurz wäre: es gibt auch keinen Anspruch darauf, dass die Gegnerin Bälle mit ihrer Noppenseite zurückspielt.\nWenn sie den Schläger dreht - Pech gehabt.",
    "date": "2018-05-01"
  },
  "FAQ9": {
    "question": "Ich habe den Ball aus Versehen doppelt berührt - Punkt für den Gegner?",
    "answer": "Kurze Antwort: nein.\n\nLaut den TT-Regeln A.10.1.7 erzielt ein Spieler einen Punkt, \"wenn sein Gegner den Ball absichtlich zweimal in Folge schlägt\" (die angesprochene \"Doppelberührung\").\nDas heißt, ein unabsichtliches doppeltes Schlagen des Balls ist kein Fehler, der zu einem Punkt führt.\n\nWas ist nun darunter zu verstehen?\n\nZunächst zur Absicht: das liegt im Ermessen der Schiedsrichterin, ohne SR muss man sich einigen.\nEin Anhaltspunkt ist insbesondere die Zeit zwischen der Doppelberührung: alles unter einer halben Sekunde ist eher unabsichtlich, alles drüber eher absichtlich.\nDas ist natürlich nur ein Anhaltspunkt und keine starre Regel.\n\nDoppeltes Schlagen umfasst dabei nicht nur den Belag, sondern auch die Schlägerhand.\nDer typischste Fall ist meist, wenn der Ball den Daumen oder Zeigefinger trifft, dann den Belag und dann mit unerreichbar seltsamem Schnitt rüberkommt.\nIn dem Fall ist das Pech für den Annehmenden, wenn keine Absicht vorliegt.",
    "see": "- [TT-Regeln A](/downloads/regeln/Internationale_Tischtennisregeln_A.pdf), 10.1.4",
    "date": "2018-06-01"
  }
}
