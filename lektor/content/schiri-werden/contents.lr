title: Schiedsrichter:in im BTTV werden
---
date: 2024-11-12
---
_template: navsite.html
---
page_order: 4
---
description: Die Berliner Schiedsrichterinnen und Schiedsrichter suchen Verstärkung.
---
hide_on_homepage: no
---
body:


<div class="card border-danger mb-3">
	<div class="card-body">
		<h4 class="card-title">VSR-Lehrgang 2024/2025</h4>
		<p class="card-text">Leider findet der Lehrgang mangels Interesse nicht statt.</p>
		<p class="card-text">Wir versuchen, Ende der Saison einen Lehrgang anzubieten.</p>
	</div>
</div>

{#
<div class="card border-info mb-3">
	<div class="card-body">
		<h4 class="card-title">VSR-Lehrgang 2024/2025</h4>
		<p class="card-text">Die Planung läuft...</p>
		<p class="card-text">Ende August.</p>
	</div>
</div>
#}

{% from "macros/image.html" import get_image_tag %}

Morgens, halb 10 in Berlin, du trägst schwarze Hose und Hemd und bist unterwegs zum Sportkomplex Paul-Heyse-Straße.
Dort findet heute das letzte Ligaspiel der 1.
Damen-Bundesliga statt und für beide Mannschaften geht es um den Einzug in die Play-Offs.
In der Sporthalle angekommen, gehst du an der Kasse vorbei, begrüßt Stadionsprecher und Vereinsvorsitzende und triffst die Oberschiedsrichterin und die Schiedsrichterkolleg:innen.
Ihr seid zu fünft und die Stimmung ist gut, die Oberschiedsrichterin hat bereits Temperatur und Licht gemessen, die Größe der Spielboxen überprüft und bereitet gerade den Spielbericht vor.
Sie bittet euch, die Netze und Zählgeräte zu überprüfen und sie dann beim Prüfen der Schläger zu unterstützen.

{% set theImage = this.attachments.images.get('VSR-Lehrgang_2023_blau_B_breit.png') %}
[{{ get_image_tag(image=theImage, alt='VSR-Lehrgang-Motivation: Bist Du voll fokussiert?', max_width=150, class='img-thumbnail float-left') }}]({{ theImage | url }})
Unmittelbar nach der Begrüßung, zu der ihr euch mit den Spielerinnen aufgestellt habt, geht es mit den Doppeln los.
Ihr seid zu zweit am Tisch, der Assistent legt die Schläger bereit, überprüft nochmal den Spielraum, Tisch und Netz und du führst mit den Spielerinnen die Wahl durch: „Aufschlag/Rückschlag oder Seite?“.
Im Augenwinkel siehst du einen Vereinskollegen im Publikum, der überrascht ist, dass das Gewinnerpaar eine bestimmte Seite auswählt, die Gegnerinnen entscheiden sich für Rückschlag.
Es ist ein großartiges Spiel, zwei-drei fehlerhafte Aufschläge müssen beanstandet werden, also je Punkt für die Rückschlagenden.

8:9 bei 1:2 im vierten Satz, die Berlinerinnen haben sich gerade nochmal ran gekämpft, jeder Punkt ist wichtig – gerade schicken sie die Gegnerin von der Rückhand weit in die Vorhand, diese schafft es aber gerade so, den Ball noch zu erreichen.
Er fliegt mit viel Spin um das Netz herum, trifft die Außenkante und fliegt im hohen Bogen auf dich zu, für die Spielerin unerreichbar.
Die Spielerinnen gucken dich irritiert an, im Publik rumort es „der war doch draußen“ und „nicht mal übers Netz gespielt“, aber du bist dir zu 98 % sicher, bei dem Winkel war der Ball an der Kante und damit auf der Seitenlinie, ob übers Netz oder am Netz vorbei spielt dabei keine Rolle.
Du blickst kurz zum Assistenten dir gegenüber, der kaum merklich nickt und zeigst mit der Hand auf die Kante, hebst den Arm „8:10“ und zeigst den nächsten Aufschlag an.
Matchball gegen Berlin denkst du noch, da landet der Ball auch schon im Netz und das Spiel ist vorbei.

{% set theImage = this.attachments.images.get('VSR-Lehrgang_2023_grün_A_breit.png') %}
[{{ get_image_tag(image=theImage, alt='VSR-Lehrgang-Motivation: Siehst Du den Ball?', max_width=150, class='img-thumbnail float-right') }}]({{ theImage | url }})
Genauso spannend geht es in den Einzeln weiter, im Wechsel bist du Assistent:in oder Schiedsrichter:in.
Dazwischen tauscht ihr euch kurz aus, erhaltet Rückmeldung von der Oberschiedsrichterin und nach gut 3 Stunden seid ihr erschöpft aber sehr zufrieden mit dem Einsatz – egal wer gewonnen hat, das Spiel war super und dein Vereinskamerad schlägt dir zum Abschied auf die Schulter "Mensch, so klasse Spiele und du bist mitten drin.
Den Kantenball hätte ich nicht gesehen und das mit dem Netz musst du mir bitte nochmal erklären".

## Ausbildung als Schiedsrichter:in

Dein Einstieg als Schiedsrichter:in ist die Ausbildung zur/zum Verbandsschiedsrichter:in (VSR).
Wer Mitglied in einem Berliner Tischtennisverein ist und mindestens 14 Jahre alt ist, kann an dem Lehrgang teilnehmen.
Dieser wird als Blended Learning, d.h. eine Mischung aus online-Inhalten und Präsenzveranstaltungen, angeboten und schließt mit einer schriftlichen, praktischen und mündlichen Prüfung ab.
Nach bestandener Prüfung bist du als Schiedsrichter:in am Tisch (SRaT) sowie als Oberschiedsrichter:in (OSR) auf Verbandsebene tätig.
In Berlin bedeutet das, direkt in den Regional-, Oberligen und Bundesligen inklusive der 1.
Bundesliga der Damen eingesetzt zu werden.

Nach einigen Jahren Einsatzerfahrung kannst du dich zur/zum Nationalen Schiedsrichter (NSR), Nationalen Oberschiedsrichter (NOSR) und später zur/zum Internationalen Schiedsrichter:in (International Umpire, ISR) auf verschiedenen Stufen weiterqualifizieren.
Dann sind auch bundesweite Einsätze bei Bundesveranstaltungen, in der TTBL und bei internationalen Turnieren möglich.
Mehr Informationen dazu findest du [beim DTTB](https://www.tischtennis.de/mein-sport/schiedsrichterin/schiedsrichter-werden.html).
Dort gibt es auch ein knackiges [Regel-Quiz](https://www.tischtennis.de/mein-sport/schiedsrichterin/schiedsrichter-werden.html), um zu sehen, ob du das Zeug zum Schiedsen hast.

(verfasst von Laura Hoßfeld)

## VSR-Lehrgang 2024/2025

{% set theImage = this.attachments.images.get('VSR-Lehrgang_2023_blau_C_breit.png') %}
[{{ get_image_tag(image=theImage, alt='VSR-Lehrgang-Motivation: Jedes Spiel braucht Regeln', max_width=150, class='img-thumbnail float-left') }}]({{ theImage | url }})
Der nächste Lehrgang findet von Mitte November 2024 bis Mitte Januar 2025 statt.
Er umfasst ein Online-Kennenlerntreffen am 20.11.2024, 18:30 – 20:30 Uhr), ein Online-gestütztes Selbststudium (internationale Tischtennisregeln, Aufgaben als OSR und SRaT, Vorgehen beim Schlägertest sowie jeweils dazugehörige Übungen), eine Hospitation und jeweils einen Online-Theorie- (18.12.24, 18.30 – 20.30 Uhr) sowie Präsenz-Praxisabend (09.01.2025 oder 15.01.2025 jeweils von 19.00 – 21.00 Uhr).
Die Prüfung findet im Rahmen der Berliner Meisterschaften der Damen und Herren am 18./19.01.2025 statt.
Wir empfehlen, an allen angebotenen Terminen des Lehrgangs teilzunehmen, verpflichtend sind jedoch nur die Online-Tests sowie die Prüfungstermine.

Die Kosten für die Teilnahme betragen 100 Euro pro Person und werden meistens von den Vereinen übernommen.
Wende dich dazu am besten an deinen Vorstand.
Die [Anmeldung](anmeldung/) ist bis zum 11.11.2024 ausschließlich über den [Seminar-Kalender](https://dttb.click-tt.de/cgi-bin/WebObjects/nuLigaTTDE.woa/wa/courseCalendar?wosid=wvMgLmgkTVR0vTt8xzUbI0&course=88991&federation=BTTV&date=2024-11-23) möglich.

[Hinweise zur Anmeldung](https://www.bettv.de/wp-content/uploads/2024/07/Anmeldung-zu-Veranstaltungen.pdf) und kostenlosen Registrierung im Seminarkalender.
Funktioniert die Registrierung nicht? Hat euer Stammverein eure Mailadresse in den Mitgliedsdaten von Click-TT hinterlegt? Infos dazu hier: <https://www.bettv.de/lehre/online-lizenz-e-mailadresse-hinzufuegen-durch-stammverein/>


## Wie umfangreich ist die Tätigkeit als Schiedsrichter:in?

{% set theImage = this.attachments.images.get('VSR-Lehrgang_2023_grün_B_breit.png') %}
[{{ get_image_tag(image=theImage, alt='VSR-Lehrgang-Motivation: Auf Augenhöhe mit den Stars', max_width=150, class='img-thumbnail float-right') }}]({{ theImage | url }})
Von den Verbandsschiedsrichter:innen wird erwartet, pro Saison bei einem Turnier im Einsatz zu sein und mindestens 3 Ligaspiele zu übernehmen.
Der Einsatzplan wird dabei bereits frühzeitig für die gesamte Saison veröffentlicht, sodass die Einsätze gut eingeplant oder wenn nötig getauscht werden können.
Hinzu kommt noch eine Fortbildung, an der du spätestens alle 2 Jahre verpflichtend teilnehmen musst, um die Lizenz aufrecht zu erhalten.
Möglichkeiten zum Austausch gibt es bei der Verbandsschiedsrichtertagung, dem Grillen im Sommer, dem Schiedsrichtervergleichskampf der Nord-Verbände und natürlich bei den Einsätzen im Team.

[Jetzt zum Lehrgang anmelden und Schiedsrichter:in im BTTV werden!](anmeldung/)

Ihr könnt gerne Werbung im Verein für den Lehrgang machen, [zur Verbandshomepage verlinken](https://www.bettv.de/jetzt-schiedsrichterin-im-bettv-werden/) oder einfach eins oder zwei der [Plakate ausdrucken](https://www.bettv.de/wp-content/uploads/2023/11/VSR-Lehrgang_Plakate.pdf) und aushängen.
