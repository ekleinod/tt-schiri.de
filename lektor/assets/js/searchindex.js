const searchIndexData = {
	"https://www.tt-schiri.de/": {
		"title": "Die Schiedsrichterinnen und Schiedsrichter des BTTV",
		"description": "",
		"content": "",
		"url": "https://www.tt-schiri.de/"
	},
	"https://www.tt-schiri.de/corona/": {
		"title": "Corona",
		"description": "Alle coronaspezifischen Informationen, hoffentlich eine temporäre Seite...",
		"content": "Alle Downloads und Nachrichten hier sind von den Webseiten des BTTV bzw. des DTTB entnommen worden. Relevant sind die offiziell veröffentlichten PDFs. Diese sind vor der Leitung eines Spiels zu lesen: Ergänzende Bestimmungen des BTTV zum DTTB-Konzept (pdf) Ergänzende Durchführungsbestimmungen für den Punktspielbetrieb des BTTV. 27. August 2020 Schutz- und Hygienekonzept DTTB (pdf) COVID 19-Schutz- und Handlungskonzept für den Tischtennissport in Deutschland. 19. Oktober 2020 COVID 19 - Hinweise Schiedsrichter (pdf) Hinweise und Empfehlungen zur Verhinderung der Ausbreitung des Corona-Virus (SARS-CoV-2) beim Einsatz als Oberschiedsrichter und Schiedsrichter 21. August 2020 COVID 19 - Regieanweisungen (pdf) COVID 19-Regieanweisungen für die Bundesspielklassen 15. Oktober 2020 Regelungen (Auszüge) OSR Infektionsschutz geht vor Regelungen in den Internationalen Tischtennis-Regeln, Wettspielordnung, Bundesspielordnung, TTBL-Spielordnung Vorschriften von ITTR, WO, BSO und TTBL-SO wegen Infektionsschutz flexibel auslegen und ggf. außer Kraft setzen Gesetze und Auflagen gehen vor Umsetzung und Einhaltung durch Heimverein Überprüfung durch OSR verantwortlich: Hygienebeauftragte - bei Ankunft im Spiellokal erfragen Abstand OSR zu anderen Personen: 1,5 Meter Mund-Nase-Schutz während Kommunikation mit OSR SRaT immer Mund-Nase-Schutz Mannschaftsbank: max. Sollstärke + 3 Personen Zählgeräte: 1 pro Person, desinfiziert Spielablauf keine Doppel alle vorgesehenen Einzel müssen ausgetragen werden zwischen zwei Tischbelegungen jeweils eine mehrminütige Pause Verzicht auf Händeschütteln oder andere Begrüßungsrituale mit Kontakt Seitenwechsel im Uhrzeigersinn um den Tisch kein Anhauchen des Balles oder Abwischen des Handschweißes am Tisch Reinigung nach jedem Mannschaftskampf: Tischoberflächen, Tischsicherungen, Tischkanten Verhalten/Organisation alle nichtspielenden Personen: Abstandsgebot von mindestens 1,5 Metern außerhalb des eigenen Sporttreibens: Mund-Nase-Schutz Wahrung eines Abstandes von 1,5 Metern auch beim Ein- und Ausgang Tische durch geeignete Maßnahmen trennen Reinigungs- bzw. Desinfektionsmittel für alle Bereiche in ausreichendem Maß nur symptomfreie Personen in der Sportstätte Kontaktnachverfolgung über Namen aller anwesenden Personen Stoßlüftung 1x pro Stunde oder dauerhaft Durchzug Verstöße OSR weist Hygienebeauftragte hin und notiert auf OSR-Bericht zunächst Ermahnung wiederholt: Heimrecht zur Einhaltung ersuchen immer noch: Abbruch durch OSR Vestöße durch Einzelpersonen = Unsportlichkeit Meldungen zum Thema Grob seit Juli 2020, weiter zurück bin ich nicht gegangen. Meldungen des BTTV Berliner Meisterschaften werden verschoben - 21.12.2020 Präsidiumsbeschluss zum Spielbetrieb - 14.12.2020 Unterstützung für Vereine - 14.12.2020 Covid 19 Pandemie Update 27.11.2020 - 27.11.2020 Jugendausschuss verschiebt die BEM der Jugend 15 und 18 nach 2021 - 27.11.2020 Corona-Saison: Bundestag verhindert Härten beim Verlust der Stammspieler-Eigenschaft - 25.11.2020 Covid 19 Pandemie Update 12.11.2020 - 12.11.2020 Ergebnis Vereinsumfrage - 04.11.2020 Vereinsumfrage per Mail gestartet - 28.10.2020 Gesamter regionaler Berliner Ligenspielbetrieb ab sofort ausgesetzt ! - 23.10.2020 Covid-19 Pandemie Update - 22.10.2020 Schutz- und Hygienekonzept für den Wettkampfspielbetrieb des BTTV - 28.08.2020 Wegfall der Doppelspiele in der Vorrunde Saison 2020/2021 - 25.08.2020 Schutz- und Hygienekonzept - 30.07.2020 DTTB – Öffnung von Tischtennis-Hallen - 25.07.2020 Mindestabstand während der sportlichen Aktivität aufgehoben - 14.07.2020 Meldungen des DTTB Damen-Bundesliga spielt weiter, 2. und 3. Liga setzen ihre Spiele im November aus - 02.11.2020 Regional- und Oberligen unterbrechen Spielbetrieb / Top 24 auf unbestimmten Termin verschoben - 30.10.2020 Fortführung des Spielbetriebs - lokale Unterbrechungen nicht ausgeschlossen - 21.10.2020 COVID-19: Regieanweisungen für DTTB-Veranstaltungen - 30.09.2020 DTTB-Beschluss: Saison 2020/2021 in den Bundesspielklassen ohne Doppel - 24.08.2020 Schutzkonzept - Update 17.8.: Doppel wieder möglich, Maskenpflicht für Nicht-Sporttreibende - 17.08.2020",
		"url": "https://www.tt-schiri.de/corona/"
	},
	"https://www.tt-schiri.de/downloads/": {
		"title": "Downloads",
		"description": "Hier findet Ihr alle Downloads, die wir zur Verfügung stellen.",
		"content": "Downloads, die hier nicht aufgelistet sind, findet Ihr mit hoher Wahrscheinlichkeit: beim BTTV Satzungen und Ordnungen beim DTTB Formulare (hauptsächlich Spielbetrieb) SR-Belange (Regelauslegungen, Handzeichen, Weiterbildung) Regeln, Satzung und Ordnungen bei der ITTF Schirizeug Schiridokumente Belaglisten (LARC) Einsatzplan Name Beschreibung Stand BTTV Einsatzplan (pdf, bunt) Einsatzplan für alle VSR, PDF-Datei, bunt. 7. November 2020 BTTV Einsatzplan (pdf, schwarz-weiß) Einsatzplan für alle VSR, PDF-Datei, schwarz-weiß. 7. November 2020 BTTV Einsatzplan (xlsx) Einsatzplan für alle VSR, Excel. 7. November 2020 Corona Name Beschreibung Stand Ergänzende Bestimmungen des BTTV zum DTTB-Konzept (pdf) Ergänzende Durchführungsbestimmungen für den Punktspielbetrieb des BTTV. 27. August 2020 Schutz- und Hygienekonzept DTTB (pdf) COVID 19-Schutz- und Handlungskonzept für den Tischtennissport in Deutschland. 19. Oktober 2020 COVID 19 - Hinweise Schiedsrichter (pdf) Hinweise und Empfehlungen zur Verhinderung der Ausbreitung des Corona-Virus (SARS-CoV-2) beim Einsatz als Oberschiedsrichter und Schiedsrichter 21. August 2020 COVID 19 - Regieanweisungen (pdf) COVID 19-Regieanweisungen für die Bundesspielklassen 15. Oktober 2020 Einsatzinformationen Name Beschreibung Stand DTTB-Handbuch DTTB-Handbuch - Auszug aus den Ordnungen. 1. August 2020 DTTB OSR-Hinweise BL Hinweise für OSR in den Bundesligen. 1. September 2020 DTTB Reisekostenabrechnung BL Reisekostenabrechnung für OSR in den Bundesligen. 27. August 2017 DTTB Einzel- und Doppelaufstellung BL Einzel- und Doppelaufstellung für 4er-Teams in den Bundesligen. 27. August 2017 DTTB OSR-Hinweise RL/OL Hinweise für OSR in den Regional- und Oberligen. 31. August 2020 DTTB Reisekostenabrechnung RL/OL Reisekostenabrechnung für OSR in den Regional- und Oberligen. 27. August 2017 DTTB Einzel- und Doppelaufstellung 4er-Teams RL/OL Einzel- und Doppelaufstellung für 4er-Teams in den Regional- und Oberligen. 27. August 2017 DTTB Einzel- und Doppelaufstellung 6er-Teams RL/OL Einzel- und Doppelaufstellung für 6er-Teams in den Regional- und Oberligen. 27. August 2017 DTTB Richtlinie zu Schlägertests 27. August 2019 Schiedsrichterzettel des DTTB 2. Juni 2007 Schiedsrichterzettel des WTTV Etwas schöner, als Download beim WTTV nicht mehr verfügbar. 22. März 2016 OSR-Berichte Name Beschreibung Stand OSR-Bericht BL Ausfüllhilfe und Empfänger siehe OSR-Berichte. 1. September 2020 OSR-Bericht RL/OL Ausfüllhilfe und Empfänger siehe OSR-Berichte. 28. August 2020 Materiallisten Name Beschreibung Stand Material 1. BL Damen Download von Click-TT. 17. August 2020 Material 2. BL Damen Download von Click-TT. 17. August 2020 Material 2. BL Herren Download von Click-TT. 17. August 2020 Material RLN Damen Download von Click-TT. 17. August 2020 Material RLN Herren Download von Click-TT. 17. August 2020 Material OLNO Damen Download von Click-TT. 17. August 2020 Material OLNO Herren Download von Click-TT. 17. August 2020 Regeln Name Beschreibung Stand Internationale TT-Regeln Teil A Teil A der Internationalen Tischtennis-Regeln. 23. Juli 2020 Internationale TT-Regeln Teil B Teil B der Internationalen Tischtennis-Regeln. 16. Februar 2020 Handzeichen und Ansagen Übersicht der Handzeichen und Ansagen der SRaT. 1. Juni 2016 Handzeichen für falsche Aufschläge Übersicht der Handzeichen für falsche Aufschläge der SRaT. 1. Juni 2016 Coaching-Regel-Tableau Offizielles Tableau mit Praxisbeispielen zur Coachingregel. 13. August 2017 Merkblatt Coaching-Regel Kurzes Merkblatt zur Coachingregel mit Praxisbeispielen. Erstellt vom VSRA des BTTV. 27. August 2017 Aktuelle Regelauslegungen Aktuelle Regelauslegungen des DTTB. 12. August 2019 Ordnungen Name Beschreibung Stand Wettspielordnung des DTTB mit Berliner Ergänzung Wettspielordnung des DTTB mit Berliner Ergänzung. 17. Juli 2019 Wettspielordnung des DTTB Die Wettspielordnung des DTTB. 21. Juli 2020 Bundesspielordnung (BSO) des DTTB Die Bundesspielordnung (BSO) des DTTB. 2. Juli 2020 VSR-Fortbildung Name Beschreibung Stand Fortbildungsfolien 3.4.2019 Folien der Fortbildung 3. April 2019 Zählgerät und Ansagen Zählgerät und Ansagen im Satz ausführlich beschrieben. 7. Dezember 2018 Zählgerät Folien über Zählgerätanzeige 7. Dezember 2018 Fortbildungsfolien 6.9.2018 Folien der Fortbildung 8. September 2018 Fortbildungsfolien 19.4.2018 Folien der Fortbildung 18. April 2018 Fortbildungsfolien 6.9.2017 Folien der Fortbildung 6. September 2017 Fortbildungsfolien 27.4.2017 Folien der Fortbildung 27. April 2017 Fortbildungsfolien 7.9.2016 Folien der Fortbildung 7. September 2016 Fortbildungsfolien 28.4.2016 Folien der Fortbildung 28. April 2016 Fortbildungsfolien 2.9.2015 Folien der Fortbildung 2. September 2015 Sonstiges Name Beschreibung Stand Checkliste OSR-Einsatz Kurze Checkliste, was zum OSR-Einsatz mitzunehmen ist. Erstellt vom VSRA des BTTV. 7. August 2019 Merkblatt OSR-Einsatz Kurzes Merkblatt über den OSR-Einsatz. Erstellt vom VSRA des BTTV. 7. August 2019 Belaglisten Name Beschreibung Stand LARC 2020 B Belagliste 2020 B, PDF-Export der tischtennis.de-Liste. (aktuell gültig) 1. Oktober 2020 LARC 2020 A Belagliste 2020 A. (aktuell gültig) 1. April 2020 LARC 2019 B Belagliste 2019 B. 1. Oktober 2019 LARC 2019 A Belagliste 2019 A. 1. April 2019 LARC 2018 B Belagliste 2018 B. 1. Oktober 2018 LARC 2018 A Belagliste 2018 A. 1. April 2018 LARC 2017 B Belagliste 2017 B. 1. Oktober 2017 LARC 2017 A Belagliste 2017 A. 1. April 2017 LARC 2016 B Belagliste 2016 B. 1. Oktober 2016 LARC 2016 A Belagliste 2016 A. 1. April 2016 LARC 2015 B Update Belagliste 2015 B Update. 4. Januar 2016 LARC 2015 B Belagliste 2015 B. 1. Oktober 2015 LARC 2015 A Belagliste 2015 A. 1. Mai 2015 LARC 35 B Belagliste 35 B. 1. Oktober 2014 LARC 35 A Belagliste 35 A. 1. Juli 2014 LARC 34 B Belagliste 34 B. 1. Januar 2014 LARC 34 A Belagliste 34 A. 1. Juli 2013 LARC 33 B Belagliste 33 B. 1. Januar 2013 LARC 33 A Belagliste 33 A. 1. Juli 2012 LARC 32 B Belagliste 32 B. 1. Januar 2012 LARC 32 A Belagliste 32 A. 1. Juli 2011 LARC 31 B Belagliste 31 B. 1. Januar 2011 LARC 31 A Belagliste 31 A. 1. Juli 2010 LARC 30 B Belagliste 30 B. 1. Januar 2010 LARC 30 A Belagliste 30 A. 1. Juni 2009 LARC 29 B Belagliste 29 B. 15. Oktober 2008 LARC 29 A Belagliste 29 A. 1. April 2008 LARC 28 B Belagliste 28 B. 1. Oktober 2007 LARC 28 A Belagliste 28 A. 1. Juli 2007 LARC 27 B Belagliste 27 B. 1. Januar 2007 LARC 27 A Belagliste 27 A. 1. Juli 2006 LARC 26 B Belagliste 26 B. 1. Januar 2006 LARC 26 A Belagliste 26 A. 1. Juli 2005 LARC 25 B Belagliste 25 B. 1. Januar 2005 LARC 25 A Belagliste 25 A. 1. Juli 2004 LARC 24 B Belagliste 24 B. 1. Januar 2004",
		"url": "https://www.tt-schiri.de/downloads/"
	},
	"https://www.tt-schiri.de/einsatzinformationen/": {
		"title": "Einsatzinformationen",
		"description": "Einsatzinformationen für die aktuelle Saison, OSR-Berichte, Downloads, Belaglisten-Archiv (LARCs)",
		"content": "OSR-Berichte Belaglisten-Archiv Kein Archiv für zurückliegende Saisons. Schiedsrichtertausch, Absage Wenn Ihr einen Einsatz tauschen wollt oder nicht kommen könnt, geht bitte wie folgt vor: besorgt einen/eine Ersatzschiedsrichter_in für Euren Termin, die Kontaktdaten findet Ihr auf der VSR-Liste gebt den Tausch per E-Mail oder Telefon bekannt an: den VSRA (Alexander Ohle, Martin Becker oder Ekkart Kleinod) den jeweiligen Ansprechpartner des ausrichtenden Vereins Eure/Euren Ersatzschiedsrichter_in fertig. falls Ihr keinen Ersatz finden könnt, informiert den VSRA (Alexander Ohle, Martin Becker oder Ekkart Kleinod) Unser Ziel ist, dass zu jedem Spiel ein Oberschiedsrichter und die Schiedsrichter pünktlich erscheinen. Bei Verhinderung haltet Euch rechtzeitig an die vorgegebenen 4 Punkte, damit gewährleistet ist, dass kein Spiel ohne OSR bzw. SR stattfinden muss. Aufwandsentschädigung Liga Tagegeld Fahrkosten pro km 1. Bundesliga Damen 30.00 € 0.30 € 2. Bundesliga Damen 25.00 € 0.30 € 3. Bundesliga Herren Nord 22.00 € 0.30 € Regionalliga Damen Nord 20.00 € 0.30 € Regionalliga Herren Nord 20.00 € 0.30 € Oberliga Nordost Damen 20.00 € 0.30 € Oberliga Nordost Herren 20.00 € 0.30 € Downloads Einsatzplan Name Beschreibung Stand BTTV Einsatzplan (pdf, bunt) Einsatzplan für alle VSR, PDF-Datei, bunt. 7. November 2020 BTTV Einsatzplan (pdf, schwarz-weiß) Einsatzplan für alle VSR, PDF-Datei, schwarz-weiß. 7. November 2020 BTTV Einsatzplan (xlsx) Einsatzplan für alle VSR, Excel. 7. November 2020 Corona Name Beschreibung Stand Ergänzende Bestimmungen des BTTV zum DTTB-Konzept (pdf) Ergänzende Durchführungsbestimmungen für den Punktspielbetrieb des BTTV. 27. August 2020 Schutz- und Hygienekonzept DTTB (pdf) COVID 19-Schutz- und Handlungskonzept für den Tischtennissport in Deutschland. 19. Oktober 2020 COVID 19 - Hinweise Schiedsrichter (pdf) Hinweise und Empfehlungen zur Verhinderung der Ausbreitung des Corona-Virus (SARS-CoV-2) beim Einsatz als Oberschiedsrichter und Schiedsrichter 21. August 2020 COVID 19 - Regieanweisungen (pdf) COVID 19-Regieanweisungen für die Bundesspielklassen 15. Oktober 2020 Einsatzinformationen Name Beschreibung Stand DTTB-Handbuch DTTB-Handbuch - Auszug aus den Ordnungen. 1. August 2020 DTTB OSR-Hinweise BL Hinweise für OSR in den Bundesligen. 1. September 2020 DTTB Reisekostenabrechnung BL Reisekostenabrechnung für OSR in den Bundesligen. 27. August 2017 DTTB Einzel- und Doppelaufstellung BL Einzel- und Doppelaufstellung für 4er-Teams in den Bundesligen. 27. August 2017 DTTB OSR-Hinweise RL/OL Hinweise für OSR in den Regional- und Oberligen. 31. August 2020 DTTB Reisekostenabrechnung RL/OL Reisekostenabrechnung für OSR in den Regional- und Oberligen. 27. August 2017 DTTB Einzel- und Doppelaufstellung 4er-Teams RL/OL Einzel- und Doppelaufstellung für 4er-Teams in den Regional- und Oberligen. 27. August 2017 DTTB Einzel- und Doppelaufstellung 6er-Teams RL/OL Einzel- und Doppelaufstellung für 6er-Teams in den Regional- und Oberligen. 27. August 2017 DTTB Richtlinie zu Schlägertests 27. August 2019 Schiedsrichterzettel des DTTB 2. Juni 2007 Schiedsrichterzettel des WTTV Etwas schöner, als Download beim WTTV nicht mehr verfügbar. 22. März 2016 OSR-Berichte Name Beschreibung Stand OSR-Bericht BL Ausfüllhilfe und Empfänger siehe OSR-Berichte. 1. September 2020 OSR-Bericht RL/OL Ausfüllhilfe und Empfänger siehe OSR-Berichte. 28. August 2020 Materiallisten Name Beschreibung Stand Material 1. BL Damen Download von Click-TT. 17. August 2020 Material 2. BL Damen Download von Click-TT. 17. August 2020 Material 2. BL Herren Download von Click-TT. 17. August 2020 Material RLN Damen Download von Click-TT. 17. August 2020 Material RLN Herren Download von Click-TT. 17. August 2020 Material OLNO Damen Download von Click-TT. 17. August 2020 Material OLNO Herren Download von Click-TT. 17. August 2020 Sonstiges Name Beschreibung Stand Checkliste OSR-Einsatz Kurze Checkliste, was zum OSR-Einsatz mitzunehmen ist. Erstellt vom VSRA des BTTV. 7. August 2019 Merkblatt OSR-Einsatz Kurzes Merkblatt über den OSR-Einsatz. Erstellt vom VSRA des BTTV. 7. August 2019 Belaglisten Name Beschreibung Stand LARC 2020 B Belagliste 2020 B, PDF-Export der tischtennis.de-Liste. (aktuell gültig) 1. Oktober 2020 LARC 2020 A Belagliste 2020 A. (aktuell gültig) 1. April 2020",
		"url": "https://www.tt-schiri.de/einsatzinformationen/"
	},
	"https://www.tt-schiri.de/einsatzinformationen/belaglisten/": {
		"title": "Belaglisten-Archiv",
		"description": "",
		"content": "Hier findet Ihr alle Belaglisten der ITTF, wenn Ihr fehlende Listen habt, lasst Sie mir bitte zukommen. LARC 2020 B Belagliste 2020 B, PDF-Export der tischtennis.de-Liste. (aktuell gültig) 1. Oktober 2020 LARC 2020 A Belagliste 2020 A. (aktuell gültig) 1. April 2020 LARC 2019 B Belagliste 2019 B. 1. Oktober 2019 LARC 2019 A Belagliste 2019 A. 1. April 2019 LARC 2018 B Belagliste 2018 B. 1. Oktober 2018 LARC 2018 A Belagliste 2018 A. 1. April 2018 LARC 2017 B Belagliste 2017 B. 1. Oktober 2017 LARC 2017 A Belagliste 2017 A. 1. April 2017 LARC 2016 B Belagliste 2016 B. 1. Oktober 2016 LARC 2016 A Belagliste 2016 A. 1. April 2016 LARC 2015 B Update Belagliste 2015 B Update. 4. Januar 2016 LARC 2015 B Belagliste 2015 B. 1. Oktober 2015 LARC 2015 A Belagliste 2015 A. 1. Mai 2015 LARC 35 B Belagliste 35 B. 1. Oktober 2014 LARC 35 A Belagliste 35 A. 1. Juli 2014 LARC 34 B Belagliste 34 B. 1. Januar 2014 LARC 34 A Belagliste 34 A. 1. Juli 2013 LARC 33 B Belagliste 33 B. 1. Januar 2013 LARC 33 A Belagliste 33 A. 1. Juli 2012 LARC 32 B Belagliste 32 B. 1. Januar 2012 LARC 32 A Belagliste 32 A. 1. Juli 2011 LARC 31 B Belagliste 31 B. 1. Januar 2011 LARC 31 A Belagliste 31 A. 1. Juli 2010 LARC 30 B Belagliste 30 B. 1. Januar 2010 LARC 30 A Belagliste 30 A. 1. Juni 2009 LARC 29 B Belagliste 29 B. 15. Oktober 2008 LARC 29 A Belagliste 29 A. 1. April 2008 LARC 28 B Belagliste 28 B. 1. Oktober 2007 LARC 28 A Belagliste 28 A. 1. Juli 2007 LARC 27 B Belagliste 27 B. 1. Januar 2007 LARC 27 A Belagliste 27 A. 1. Juli 2006 LARC 26 B Belagliste 26 B. 1. Januar 2006 LARC 26 A Belagliste 26 A. 1. Juli 2005 LARC 25 B Belagliste 25 B. 1. Januar 2005 LARC 25 A Belagliste 25 A. 1. Juli 2004 LARC 24 B Belagliste 24 B. 1. Januar 2004",
		"url": "https://www.tt-schiri.de/einsatzinformationen/belaglisten/"
	},
	"https://www.tt-schiri.de/einsatzinformationen/osr-berichte/": {
		"title": "OSR-Berichte",
		"description": "",
		"content": "Zu jedem Spiel, das Ihr als OSR leitet, ist ein OSR-Bericht zu erstellen. Auf dieser Seite sind alle Informationen zusammengefasst, die dafür nötig sind. Kurzanleitung Folgende Schritte sind für einen korrekten OSR-Bericht vorzunehmen: OSR-Bericht des DTTB herunterladen (Anleitung) OSR-Bericht korrekt benennen (Anleitung) OSR-Bericht ausfüllen (Anleitung) OSR-Bericht abspeichern (Anleitung) OSR-Bericht per Mail versenden (Anleitung und Adressen) OSR-Bericht des DTTB herunterladen Ladet den für die Spielklasse passenden OSR-Bericht herunter. OSR-Bericht BL Ausfüllhilfe und Empfänger siehe OSR-Berichte. 1. September 2020 OSR-Bericht RL/OL Ausfüllhilfe und Empfänger siehe OSR-Berichte. 28. August 2020 Speichert den OSR-Bericht am besten auf die Festplatte und füllt ihn dort aus. Wenn der OSR-Bericht direkt im Browser geöffnet wird, geht zurück, klickt den Link mit der rechten Maustaste an und wählt Ziel speichern unter. In anderen Worten: Bei mir selbst erscheint die gleiche Meldung [...] Allerdings bestätige ich dann rechts oben nochmal das Öffnen und wähle den Adobe-Reader aus. Dann öffnet sich der OSR-Bericht. OSR-Bericht korrekt benennen Benennt den heruntergeladenen Bericht bitte nach folgendem Namensschema: OSR_Nennung der Spielklasse_Damen/Herren_Spielnummer_Begegnung.pdf Beispiele: OSR_BL_Da1_05_Eastside-Böblingen.pdfOSR_BL_He3_03_Hertha-Seligenstadt.pdfOSR_RL_DaNord_01_Füchse-Göttingen.pdfOSR_RL_HeNord_02_Düppel-Tündern.pdfOSR_OL_DaNordOst_12_Neukölln-Ahrensberg.pdfOSR_OL_HeNordOst_13_Spandau-Sasel.pdf OSR-Bericht ausfüllen Beim Ausfüllen des OSR-Berichts gibt es zwei Phasen: Daten erheben (beim Spiel) Daten in PDF-Formular übertragen (am Computer) Da ich selten einen Computer beim Spiel dabeihabe, drucke ich für jedes Spiel einen OSR-Bericht aus, den ich während des Spiels ausfülle. Diese Daten übernehme ich dann zu Hause in den Computer. Am Computer öffnet Ihr die PDF-Datei mit dem Acrobat Reader. Es gibt zwar gute und kostenlose Alternativen zum Adobe Reader (siehe Wikipedia) mit diesen funktionieren die neuen Formulare jedoch nicht immer. PDFXChangeViewer soll funktionieren. Nachdem Ihr die PDF-Datei geöffnet habt, tragt Ihr die Daten des Spiels in das Formular ein. Um sicherzugehen, dass Ihr die Eintragungen abspeichern und wieder laden können, tragt zunächst nur wenige Daten ein, z.B. die Heimmannschaft. Speichert die Datei ab und schließt den PDF-Reader. Öffnet jetzt die gespeicherte PDF-Datei. Wenn die Heimmannschaft immer noch eingetragen ist, funktioniert das Speichern. Wenn nicht, verwendet einen anderen PDF-Reader. Das Formular ist relativ gut aufgebaut, daher nur ein paar wichtige Punkte, die oft falsch eingetragen werden: Spielnummer nicht vergessen Einsatzplan (pdf) bzw. Einsatzplan (xls) elektronisches Zählgerät in der Box: wirklich nur, wenn in der Box welche vorhanden waren, es ist nicht der Gesamtspielstandsanzeiger gemeint OSR-Lizenz ankreuzen Mannschaftsführer namentlich eintragen Kreuz bei Gastverein hat das Trikot gewechselt... nur dann setzen (ja oder nein), wenn die Trikots gleichfarbig waren. Im Normalfall hier also kein Kreuz (weder bei ja noch bei nein) setzen. Schläger müssen vor dem Spiel überprüft werden (Sichtprüfung) Messgerätprüfung nur, wenn man eins hat bei den Gründen für Karten: genaue Angabe, also nicht nur Unsportlichkeit, sondern Treten gegen den Tisch OSR-Bericht abspeichern Jetzt speichert den OSR-Bericht ab. Da Ihr das oben schon getestet haben, sollten Bericht und Daten gespeichert sein. Ihr könnt das überprüfen, indem Ihr das Dokument schließt und wieder öffnet. Jetzt müssen die eingetragenen Daten noch vorhanden sein. Prüft bitte, ob die Daten nach einmal Programm schließen und wieder öffnen noch sichtbar sind, damit Ihr keinen leeren OSR-Bericht verschickt. OSR-Bericht per E-Mail versenden Der OSR-Bericht muss innerhalb von 48 Stunden an den jeweiligen Spielleiter und an den VSRO (Ekkart Kleinod) versendet werden. Prüft noch einmal, ob alle Daten eingetragen sind (einfach noch einmal durchlesen oder noch besser: jemandem zum Durchlesen geben). Jetzt sendet den OSR-Bericht an alle Empfänger, wie das genau geschieht, hängt von Eurem PDF-Reader und Eurem Mailprogramm ab, so dass es schwierig ist, eine allgemeine Anleitung zu geben. Grundsätzlich verfasst Ihr eine neue E-Mail, tragt die E-Mailempfänger ein, hängt den OSR-Bericht an und schickt die E-Mail ab. 1. Bundesliga Damen Gabi Klis: klis.dttb@tischtennis.de André Zickert: zickert.dttb@tischtennis.de Ekkart Kleinod: schiri@ekkart.de 2. Bundesliga Damen Ekkart Kleinod: schiri@ekkart.de Gabi Klis: klis.dttb@tischtennis.de Kolja Rottmann: rottmann.dttb@tischtennis.de (d)) 3. Bundesliga Herren Nord Gabi Klis: klis.dttb@tischtennis.de André Zickert: zickert.dttb@tischtennis.de Ekkart Kleinod: schiri@ekkart.de Regionalliga Damen Nord Andreas Ahlers: mail@ahlers-tt.de Ekkart Kleinod: schiri@ekkart.de Regionalliga Herren Nord Lothar Fricke: fricke@ttvn.de Ekkart Kleinod: schiri@ekkart.de Oberliga Nordost Damen Andreas Ahlers: mail@ahlers-tt.de Ekkart Kleinod: schiri@ekkart.de Oberliga Nordost Herren Andreas Ahlers: mail@ahlers-tt.de Ekkart Kleinod: schiri@ekkart.de",
		"url": "https://www.tt-schiri.de/einsatzinformationen/osr-berichte/"
	},
	"https://www.tt-schiri.de/impressum/": {
		"title": "Impressum",
		"description": "",
		"content": "Dies sind die Seiten der Schiedsrichterinnen und Schiedsrichter des Berliner Tisch-Tennis Verbands. Verantwortlich für die Webseite ist Ekkart Kleinod schiri@ekkart.de Programmierung, Anpassungen Die Umsetzung der Webseite erfolgt durch: Ekkart Kleinod gitlab: ekleinod E-Mail: schiri@ekkart Tobias Kantusch github: tkantusch E-Mail: tobiaskantusch@online.de Der Code (und die Inhalte) sind abrufbar unter: https://gitlab.com/ekleinod/tt-schiri.de Folgende externe Dinge wurden für die Webseite genutzt bzw. angepasst Lektor statischer Webseitengenerator https://www.getlektor.com/ Bootswatch-Templates bootstrap-Templates https://bootswatch.com/yeti/ Bootstrap Framework für responsive Webseiten https://getbootstrap.com/ Font Awesome Icon-Font http://fontawesome.io/ jQuery JavaScript-Bibliothek https://jquery.com/ Lunr JavaScript-Suche https://lunrjs.com/",
		"url": "https://www.tt-schiri.de/impressum/"
	},
	"https://www.tt-schiri.de/personen/": {
		"title": "Ansprechpartner",
		"description": "Der wichtigste Ansprechpartner für Verbandsschiedsrichter und für Fragen rund um Schiedsrichter und Regeln ist der Verbandsschiedsrichterausschuss (VSRA).",
		"content": "Verbandsschiedsrichterausschuss Der VSRA ist die Vertretung der Schiedsrichter beim Berliner Tisch-Tennis Verband. Die Sammelmailadresse des VSRA, die an alle VSRA-Mitglieder gleichzeitig geht, ist: vsrausschuss@bettv.de Vorsitzender Ekkart Kleinod E-Mail: schiri@ekkart.de Telefon: findet Ihr in der VSR-Liste Stellvertreter Martin Becker E-Mail: martin.becker@bettv.de Telefon: findet Ihr in der VSR-Liste Beisitzer Alexander Ohle E-Mail: a.ohle@gmx.de Telefon: findet Ihr in der VSR-Liste Berliner Tisch-Tennis Verband Präsidium & Funktionäre Geschäftsstelle Silvio Herbig (Geschäftsführer) silvio.herbig@bettv.de Britta Gutschmann (Mitarbeiterin) britta.gutschmann@bettv.de",
		"url": "https://www.tt-schiri.de/personen/"
	},
	"https://www.tt-schiri.de/regeln/": {
		"title": "Regeln, Ordnungen",
		"description": "Regeln, Ordnungen und alles, was dazu gehört.",
		"content": "Regelecke Interessante Fragen mit deren Antworten findet Ihr in der Regelecke. Regeln Name Beschreibung Stand Internationale TT-Regeln Teil A Teil A der Internationalen Tischtennis-Regeln. 23. Juli 2020 Internationale TT-Regeln Teil B Teil B der Internationalen Tischtennis-Regeln. 16. Februar 2020 Handzeichen und Ansagen Übersicht der Handzeichen und Ansagen der SRaT. 1. Juni 2016 Handzeichen für falsche Aufschläge Übersicht der Handzeichen für falsche Aufschläge der SRaT. 1. Juni 2016 Coaching-Regel-Tableau Offizielles Tableau mit Praxisbeispielen zur Coachingregel. 13. August 2017 Merkblatt Coaching-Regel Kurzes Merkblatt zur Coachingregel mit Praxisbeispielen. Erstellt vom VSRA des BTTV. 27. August 2017 Aktuelle Regelauslegungen Aktuelle Regelauslegungen des DTTB. 12. August 2019 Ordnungen Name Beschreibung Stand Wettspielordnung des DTTB mit Berliner Ergänzung Wettspielordnung des DTTB mit Berliner Ergänzung. 17. Juli 2019 Wettspielordnung des DTTB Die Wettspielordnung des DTTB. 21. Juli 2020 Bundesspielordnung (BSO) des DTTB Die Bundesspielordnung (BSO) des DTTB. 2. Juli 2020",
		"url": "https://www.tt-schiri.de/regeln/"
	},
	"https://www.tt-schiri.de/regeln/faq/": {
		"title": "Regelecke",
		"description": "",
		"content": "Veraltete Antworten bitte per E-Mail melden. Danke. Im Doppel verletzt sich ein Spieler so, dass er seine Einzel nicht bestreiten kann. Muss in der Einzelaufstellung aufgerückt werden? Es kann aufgerückt werden, da die endgültige Einzelaufstellung erst nach den Anfangsdoppeln erfolgen muss. Es braucht nicht aufgerückt zu werden, da der Spieler im Mannschaftskampf mitgewirkt hat und nicht als ausgefallen gilt. Siehe WO des BTTV, D.3 WO des DTTB, D.3 Antwort vom 17.12.2016 Kann eine Einzelaufstellung noch abgeändert werden, wenn das erste Einzel bereits begonnen hat, während ein Doppel noch läuft, und wann beginnt in diesem Sinne ein Einzel? Eine Mannschaft, die es zulässt, dass das erste Einzel beginnt, während noch ein Doppel läuft, verzichtet damit auf das ihr an sich zustehende Recht einer Aufstellungsänderung. Ein Spiel beginnt im Sinne dieser Bestimmung mit dem ersten Aufschlag. Antwort vom 17.12.2016 Ein Ball geht im Ballwechsel kaputt - gibt es Wiederholung oder Punkt? Seit der Einführung der Plastikbälle nicht selten - Spielerin A spielt den Ball, er fliegt über den Tisch hinaus. Beim Aufheben stellt Spielerin B fest, dass der Ball kaputt ist. Wiederholung oder Punkt für B? Hier hat sich die Regelauslegung 2017 geändert. Es gilt bemerkt der Schiri den defekten Ball während des Ballwechsels, gibt es immer Wiederholung wird der defekte Ball erst nach dem Ballwchsel festgestellt, gibt es nur Wiederholung, wenn beide Spielerinnen das fordern, ansonsten wird auf Punkt entschieden Im Beispiel wäre also Punkt für Spielerin B die richtige Antwort. Antwort vom 01.12.2017 Eine Spielerin ist verletzt, wenn sie nicht mitspielt gibt es Strafe und die anderen müssen aufrücken, Ersatz ist nicht in Sicht. Wie kann man nun korrekt aufstellen? Die Wettspielordnung sagt: Ein Spieler hat an einem Mannschaftskampf mitgewirkt, wenn er zu mindestens einem Einzel oder Doppel antritt und dieses auch in die Wertung eingeht. Eine Mitwirkung ist schon dann gegeben, wenn der aufgestellte Spieler bei der Begrüßung anwesend ist. Das heißt, entweder ist die Spielerin bei der Begrüßung anwesend und geht dann wieder (oder schaut zu). Oder sie kommt zu ihren Doppel oder Einzel, ein Aufschlag wird gespielt und sie gibt das Spiel auf. Wichtig: im zweiten Fall muss das Einzel bzw. Doppel in die Wertung eingehen, also mindestens begonnen werden. Siehe WO des BTTV, E.4.1 Antwort vom 01.01.2018 Man muss ein Spiel aufgeben, kann nicht antreten oder ist nur zur Aufstellung anwesend - was bedeutet das für die LivePZ bzw. den TTR-Wert? Vorausgeschickt sei: die folgenden Ausführungen gelten für Mannschaftsspiele in Mannschaftswettbewerben, nicht für Turniere oder Pokal. Die Wettspielordnung des DTTB regelt das für TTR-Werte, diese Regelungen gelten analog für die LivePZ-Werte des Berliner Tischtennisverbands. Für Einzelspiele werden vier Fälle unterschieden, dazu wird festgelegt, ob die Spiele bei der LPZ-Berechnung berücksichtigt werden: Einzel, bei denen ein Spieler aufgegeben hat: werden berücksichtigt. Einzel, bei denen ein Spieler auf das Spiel verzichtet hat: werden berücksichtigt; der Spieler muss im Spielberichtsbogen eingetragen worden sein Einzel, bei denen ein Spieler nicht angetreten ist: werden nicht berücksichtigt; der Spieler darf nicht benannt worden sein, also nicht im Spielberichtsbogen eingetragen sein Einzel, die wegen Regelverstoßes umgewertet worden sind: werden wie gewertet berücksichtigt Der Vollständigkeit halber seien noch die Problemfälle für Spieler benannt, die aus Wertungen für Mannschaften resultieren: Einzel aus Mannschaftskämpfen zurückgezogener Mannschaften: werden berücksichtigt Einzel aus Mannschaftskämpfen gestrichener Mannschaften: werden berücksichtigt Einzel aus wegen Nichtantretens kampflos gewerteten Mannschaftskämpfen: werden nicht berücksichtigt Einzel aus wegen Regelverstoßes umgewerteten Mannschaftskämpfen: werden wie gespielt berücksichtigt Siehe WO des BTTV, E.3.1 Antwort vom 01.02.2018 Wir kennen das - die Zeit drängt, das Punktspiel zieht sich, dürfte eigentlich an drei Tischen weitergespielt werden? Muss an zwei Tischen begonnen werden? Hier gab es in dieser Saison Änderungen, da der DTTB die Wettspielordnung für den gesamten Bereich des DTTB, also auch den Berliner Verband geändert hat. Grundsätzlich gilt (wie bisher): Sechser- und Vierer-Mannschaften spielen grundsätzlich an zwei Tischen. Es gibt jetzt zwei Ausnahmen, die in Berlin zugelassen sind, also angewendet werden dürfen: die Heimmannschaft [darf] die Anzahl der Spieltische ohne Zustimmung der Gastmannschaft um einen erhöhen Erhöhungen der Tischanzahl [sind] im Einvernehmen beider Mannschaften zulässig Das heißt, die Heimmannschaft darf immer bestimmen, dass an einem Tisch mehr gespielt wird. Die Gastmannschaft durfte das bisher auch fordern, das wurde gestrichen, jetzt müssen sich Heim- und Gastmannschaft einig sein. Es wird insbesondere nichts dazu gesagt, wann erhöht werden kann, man kann also ein Punktspiel auch an drei Tischen beginnen. Siehe WO des BTTV, I.5.8 Antwort vom 01.03.2018 Aus Versehen den Tisch verrutscht – wie geht's weiter? Grundsätzlich gilt: Tisch verrutschen ist nicht erlaubt, ob absichtlich oder versehentlich, geschieht das im Ballwechsel, gibt es Punkt für die Gegnerin. Dann wird der Tisch wieder geradegerückt, das Netz neu ausgemessen und weitergespielt. Bei myTischtennis lag der Fall etwas anders: Der Ball von Spieler A hat gerade die Grundlinie Richtung Aus überquert, als Spieler B aus Versehen den Tisch verrutscht. Entscheidend ist jetzt: geschieht das Verrutschen des Tischs während des Ballwechsels oder danach? Regel 10.1.4 besagt: Ein Spieler erzielt einen Punkt, wenn der Ball sein Spielfeld oder seine Grundlinie passiert, ohne sein Spielfeld zu berühren, nachdem er von seinem Gegner geschlagen wurde. Das heißt, mit dem Passieren des Balls der Grundlinie erzielt Spieler B einen Punkt (mal von dem sehr theoretischen Fall abgesehen, dass der überstarke Unterschnitt oder der Wind den Ball wieder zurück auf das Spielfeld treibt). Ob er danach den Tisch verrutscht ist unerheblich, genauso wie des Aufhalten oder Auffangen des Balls. Die Regel ist hier eindeutig, es war etwas verwirrend, dass myTischtennis das anfänglich falsch beantwortete, aber Ihr wisst es jetzt besser. Siehe TT-Regeln A, 10.1.4 Antwort vom 01.04.2018 Muss sich meine Gegnerin mit mir einspielen? Kurz gesagt: nein. Die Einspielzeit von zwei Minuten ist eine Kann-Bestimmung für die Spielerinnen, das heißt, sie kann genutzt werden, muss aber nicht. Wenn eine Spielerin nicht will, geht das Spiel sofort los. Das heißt insbesondere auch, dass nicht einfach zwei Minuten mit einer Mannschaftskameradin gespielt werden darf. Und weil der Text sonst so kurz wäre: es gibt auch keinen Anspruch darauf, dass die Gegnerin Bälle mit ihrer Noppenseite zurückspielt. Wenn sie den Schläger dreht - Pech gehabt. Antwort vom 01.05.2018 Ich habe den Ball aus Versehen doppelt berührt - Punkt für den Gegner? Kurze Antwort: nein. Laut den TT-Regeln A.10.1.7 erzielt ein Spieler einen Punkt, wenn sein Gegner den Ball absichtlich zweimal in Folge schlägt (die angesprochene Doppelberührung). Das heißt, ein unabsichtliches doppeltes Schlagen des Balls ist kein Fehler, der zu einem Punkt führt. Was ist nun darunter zu verstehen? Zunächst zur Absicht: das liegt im Ermessen der Schiedsrichterin, ohne SR muss man sich einigen. Ein Anhaltspunkt ist insbesondere die Zeit zwischen der Doppelberührung: alles unter einer halben Sekunde ist eher unabsichtlich, alles drüber eher absichtlich. Das ist natürlich nur ein Anhaltspunkt und keine starre Regel. Doppeltes Schlagen umfasst dabei nicht nur den Belag, sondern auch die Schlägerhand. Der typischste Fall ist meist, wenn der Ball den Daumen oder Zeigefinger trifft, dann den Belag und dann mit unerreichbar seltsamem Schnitt rüberkommt. In dem Fall ist das Pech für den Annehmenden, wenn keine Absicht vorliegt. Siehe TT-Regeln A, 10.1.4 Antwort vom 01.06.2018",
		"url": "https://www.tt-schiri.de/regeln/faq/"
	},
	"https://www.tt-schiri.de/schiri-werden/": {
		"title": "Schiri werden",
		"description": "Die Berliner Schiedsrichterinnen und Schiedsrichter suchen Verstärkung. Vielleicht wäre das was für Dich?",
		"content": "Wir überarbeiten gerade das Konzept für die Lehrgänge und bitten um etwas Geduld.",
		"url": "https://www.tt-schiri.de/schiri-werden/"
	},
	"https://www.tt-schiri.de/termine/": {
		"title": "Termine",
		"description": "Termine und Turniere der Saison sowie Veranstaltungsorte.",
		"content": "Turniere VSR-Termine Veranstaltungsorte",
		"url": "https://www.tt-schiri.de/termine/"
	},
	"https://www.tt-schiri.de/termine/clubvenues/": {
		"title": "Spielorte",
		"description": "",
		"content": "Achtung: hier müsste noch was hin.",
		"url": "https://www.tt-schiri.de/termine/clubvenues/"
	},
	"https://www.tt-schiri.de/termine/events/": {
		"title": "VSR-Termine",
		"description": "",
		"content": "21.04.2020 VSR-Tagung",
		"url": "https://www.tt-schiri.de/termine/events/"
	},
	"https://www.tt-schiri.de/termine/events/2020-04-21_vsr-tagung/": {
		"title": "VSR-Tagung",
		"description": "",
		"content": "Dienstag, 21. April 2020 Zeit 19:00 – 21:00 Uhr Ort Clubraum des Berliner TSC, Paul-Heyse-Straße 25, 10407 Berlin Ansprechpartner Ekkart Kleinod; Martin Becker; Alexander Ohle",
		"url": "https://www.tt-schiri.de/termine/events/2020-04-21_vsr-tagung/"
	},
	"https://www.tt-schiri.de/termine/games/": {
		"title": "VSR-Einsätze",
		"description": "",
		"content": "BTTV Einsatzplan (pdf, bunt) Einsatzplan für alle VSR, PDF-Datei, bunt. 7. November 2020 BTTV Einsatzplan (pdf, schwarz-weiß) Einsatzplan für alle VSR, PDF-Datei, schwarz-weiß. 7. November 2020 BTTV Einsatzplan (xlsx) Einsatzplan für alle VSR, Excel. 7. November 2020 Hier kommt noch mehr, Geduld.",
		"url": "https://www.tt-schiri.de/termine/games/"
	},
	"https://www.tt-schiri.de/termine/tournaments/": {
		"title": "Turniere",
		"description": "",
		"content": "Verlegung bzw. abgesagte Turniere Ich hab alle zukünftigen Turniere als abgesagt markiert, bis klar ist, ob und wann sie gespielt werden. 15.08.2020 – 16.08.2020 1. VRL D/H, Jugend 13 / 15 / 18 22.08.2020 – 23.08.2020 2. VRL D/H, Jugend 13 / 15 / 18 05.09.2020 – 06.09.2020 LRL Jugend 13/18 06.09.2020 Quali Damen-Pokal (Alexander Ohle) 19.09.2020 LRL Damen und Herren 19.09.2020 – 20.09.2020 LRL Jugend 15 26.09.2020 – 27.09.2020 ND Rangliste B-Schüler:innen (Frank Röbisch) 09.10.2020 – 11.10.2020 Europe Youth Top 10 05.12.2020 – 06.12.2020 BEM Jugend 15/18 abgesagt 09.01.2021 – 10.01.2021 BEM Leistungsklassen Damen und Herren abgesagt 23.01.2021 – 24.01.2021 BEM Damen und Herren abgesagt 30.01.2021 – 31.01.2021 BEM Senioren und Seniorinnen abgesagt 06.02.2021 – 07.02.2021 QV DEM Region 6 Damen / Herren abgesagt 27.03.2021 BTTV FinalFour Verbandsklassen abgesagt 24.04.2021 – 25.04.2021 BTTV-Relegationen abgesagt 04.05.2021 – 08.05.2021 Bundesfinale JtfO, JtfP abgesagt 08.05.2021 – 09.05.2021 BEM Jugend 13 abgesagt 29.05.2021 – 30.05.2021 mini-Meisterschaften Bestenspiele Jugend 11 abgesagt 06.06.2021 Pokalfinale abgesagt",
		"url": "https://www.tt-schiri.de/termine/tournaments/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2020-08-15_1__vrl_d_h__jugend_13___15___18/": {
		"title": "1. VRL D/H, Jugend 13 / 15 / 18",
		"description": "",
		"content": "Zusammenfassung Samstag, 15. August 2020, Spielhalle Sonntag, 16. August 2020, Spielhalle Organisatorisches Ausschreibung (PDF) https://www.bettv.de/wp-content/uploads/2020/08/2020-Ausschreibung-1.-Vorrangliste.pdf BTTV-News https://www.bettv.de/ausschreibungen-erste-und-zweite-vorrangliste/ Samstag, 15. August 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 16. August 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2020-08-15_1__vrl_d_h__jugend_13___15___18/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2020-08-22_2__vrl_d_h__jugend_13___15___18/": {
		"title": "2. VRL D/H, Jugend 13 / 15 / 18",
		"description": "",
		"content": "Zusammenfassung Samstag, 22. August 2020, Spielhalle Sonntag, 23. August 2020, Spielhalle Organisatorisches Ausschreibung (PDF) https://www.bettv.de/wp-content/uploads/2020/08/2020-Ausschreibung-2.-Vorrangliste.pdf BTTV-News https://www.bettv.de/ausschreibungen-erste-und-zweite-vorrangliste/ Samstag, 22. August 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 23. August 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2020-08-22_2__vrl_d_h__jugend_13___15___18/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2020-09-05_lrl_jugend_13_18/": {
		"title": "LRL Jugend 13/18",
		"description": "",
		"content": "Zusammenfassung Samstag, 5. September 2020, Spielhalle Sonntag, 6. September 2020, Spielhalle Organisatorisches Noch keine Informationen vorhanden. Samstag, 5. September 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 6. September 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2020-09-05_lrl_jugend_13_18/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2020-09-06_quali_damen-pokal/": {
		"title": "Quali Damen-Pokal",
		"description": "",
		"content": "Oberschiedsrichter_in Alexander Ohle OSR vom DTTB, Minimum 8 NSR Organisatorisches Spielplan https://www.ttc-berlin-eastside.de/1-Damen/Spielplaene-Saison-2020/21/Deutsche-Pokalmeisterschaften Sonntag, 6. September 2020 Zeit 10:30 – 18:00 Uhr SR-Einweisung 10:00 Uhr Ort ASP.5-Sporthalle, Anton-Saefkow Platz 5, 10369 Berlin Schiedsrichter_in am Tisch (fehlend: 6) Michael Kirch Stefan Labitzke Arne Nawrath Gerd Paulke Frank Röbisch Ludolf Sonnabend Schlägerkontrolle Jurij Richter ASP.5-Sporthalle Anton-Saefkow Platz 5, 10369 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2020-09-06_quali_damen-pokal/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2020-09-19_lrl_damen_und_herren/": {
		"title": "LRL Damen und Herren",
		"description": "",
		"content": "Organisatorisches Noch keine Informationen vorhanden. Samstag, 19. September 2020 Zeit 08:30 – 17:00 Uhr Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2020-09-19_lrl_damen_und_herren/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2020-09-19_lrl_jugend_15/": {
		"title": "LRL Jugend 15",
		"description": "",
		"content": "Zusammenfassung Samstag, 19. September 2020, Spielhalle Sonntag, 20. September 2020, Spielhalle Organisatorisches Noch keine Informationen vorhanden. Samstag, 19. September 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 20. September 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2020-09-19_lrl_jugend_15/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2020-09-26_nd_rangliste_b-schueler_innen/": {
		"title": "ND Rangliste B-Schüler:innen",
		"description": "",
		"content": "Oberschiedsrichter_in Frank Röbisch Zusammenfassung Samstag, 26. September 2020, 10:00 – 18:00 Uhr, Spielhalle Sonntag, 27. September 2020, 08:30 – 13:00 Uhr, Spielhalle 10 Tische Organisatorisches Turnierinfos in Click-TT https://dttb.click-tt.de/cgi-bin/WebObjects/nuLigaTTDE.woa/wa/tournamentCalendarDetail?tournament=427008&federation=DTTB Samstag, 26. September 2020 Zeit 10:00 – 18:00 Uhr Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in Frank Röbisch Sonntag, 27. September 2020 Zeit 08:30 – 13:00 Uhr Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in Frank Röbisch Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2020-09-26_nd_rangliste_b-schueler_innen/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2020-10-09_europe_youth_top_10/": {
		"title": "Europe Youth Top 10",
		"description": "",
		"content": "Zusammenfassung Freitag, 9. Oktober 2020, 09:30 – 19:30 Uhr, ASP.5-Sporthalle Samstag, 10. Oktober 2020, 09:30 – 19:30 Uhr, ASP.5-Sporthalle Sonntag, 11. Oktober 2020, 09:30 – 19:30 Uhr, ASP.5-Sporthalle SR-Einsätze werden vom DTTB organisiert Organisatorisches BeTTV-Ankündigung https://www.bettv.de/europe-youth-top-10-vom-09-11-20-2020-in-berlin/ Offizielle Webseite https://eytt.berlin Freitag, 9. Oktober 2020 Zeit 09:30 – 19:30 Uhr Ort ASP.5-Sporthalle, Anton-Saefkow Platz 5, 10369 Berlin Samstag, 10. Oktober 2020 Zeit 09:30 – 19:30 Uhr Ort ASP.5-Sporthalle, Anton-Saefkow Platz 5, 10369 Berlin Sonntag, 11. Oktober 2020 Zeit 09:30 – 19:30 Uhr Ort ASP.5-Sporthalle, Anton-Saefkow Platz 5, 10369 Berlin ASP.5-Sporthalle Anton-Saefkow Platz 5, 10369 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2020-10-09_europe_youth_top_10/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2020-12-05_bem_jugend_15_18/": {
		"title": "BEM Jugend 15/18",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Zusammenfassung Samstag, 5. Dezember 2020, Spielhalle Sonntag, 6. Dezember 2020, Spielhalle Organisatorisches Noch keine Informationen vorhanden. Samstag, 5. Dezember 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 6. Dezember 2020 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2020-12-05_bem_jugend_15_18/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-01-09_bem_leistungsklassen_damen_und_herren/": {
		"title": "BEM Leistungsklassen Damen und Herren",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 9. Januar 2021, Spielhalle, es fehlen noch Meldungen Sonntag, 10. Januar 2021, Spielhalle, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 9. Januar 2021 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 10. Januar 2021 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-01-09_bem_leistungsklassen_damen_und_herren/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-01-09_ttt_doppel/": {
		"title": "TTT Doppel",
		"description": "",
		"content": "Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 9. Januar 2021, es fehlen noch Meldungen Sonntag, 10. Januar 2021, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 9. Januar 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1) Sonntag, 10. Januar 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1)",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-01-09_ttt_doppel/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-01-23_bem_damen_und_herren/": {
		"title": "BEM Damen und Herren",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 23. Januar 2021, 08:30 – 21:00 Uhr, Spielhalle, es fehlen noch Meldungen Sonntag, 24. Januar 2021, 08:30 – 16:00 Uhr, Spielhalle, es fehlen noch Meldungen Das große Turnier... Organisatorisches Noch keine Informationen vorhanden. Samstag, 23. Januar 2021 Zeit 08:30 – 21:00 Uhr Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Stellvertretende_r Oberschiedsrichter_in (fehlend: 1) Schiedsrichter_in am Tisch (fehlend: 22) Malte Beeker Walter Kaschubat Gerd Paulke Sonntag, 24. Januar 2021 Zeit 08:30 – 16:00 Uhr Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Stellvertretende_r Oberschiedsrichter_in (fehlend: 1) Schiedsrichter_in am Tisch (fehlend: 23) Walter Kaschubat Gerd Paulke Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-01-23_bem_damen_und_herren/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-01-30_bem_senioren_und_seniorinnen/": {
		"title": "BEM Senioren und Seniorinnen",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 30. Januar 2021, Spielhalle, es fehlen noch Meldungen Sonntag, 31. Januar 2021, Spielhalle, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 30. Januar 2021 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 31. Januar 2021 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-01-30_bem_senioren_und_seniorinnen/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-02-06_qv_dem_region_6_damen___herren/": {
		"title": "QV DEM Region 6 Damen / Herren",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Zusammenfassung Samstag, 6. Februar 2021 Sonntag, 7. Februar 2021 Keine Ahnung, ob die wieder in Berlin stattfindet... Organisatorisches Noch keine Informationen vorhanden. Samstag, 6. Februar 2021 Zeit unbekannt Ort unbekannt Sonntag, 7. Februar 2021 Zeit unbekannt Ort unbekannt",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-02-06_qv_dem_region_6_damen___herren/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-02-13_ttt_quali_1/": {
		"title": "TTT Quali 1",
		"description": "",
		"content": "Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 13. Februar 2021, es fehlen noch Meldungen Sonntag, 14. Februar 2021, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 13. Februar 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1) Sonntag, 14. Februar 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1)",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-02-13_ttt_quali_1/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-02-27_ttt_quali_2/": {
		"title": "TTT Quali 2",
		"description": "",
		"content": "Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 27. Februar 2021, es fehlen noch Meldungen Sonntag, 28. Februar 2021, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 27. Februar 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1) Sonntag, 28. Februar 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1)",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-02-27_ttt_quali_2/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-03-13_ttt_mannschaft/": {
		"title": "TTT Mannschaft",
		"description": "",
		"content": "Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 13. März 2021, es fehlen noch Meldungen Sonntag, 14. März 2021, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 13. März 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1) Sonntag, 14. März 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1)",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-03-13_ttt_mannschaft/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-03-20_ttt_quali_3/": {
		"title": "TTT Quali 3",
		"description": "",
		"content": "Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 20. März 2021, es fehlen noch Meldungen Sonntag, 21. März 2021, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 20. März 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1) Sonntag, 21. März 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1)",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-03-20_ttt_quali_3/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-03-27_bttv_finalfour_verbandsklassen/": {
		"title": "BTTV FinalFour Verbandsklassen",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Es fehlen noch Meldungen. Details siehe unten. Organisatorisches Noch keine Informationen vorhanden. Samstag, 27. März 2021 Zeit unbekannt Ort GT-Halle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) GT-Halle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-03-27_bttv_finalfour_verbandsklassen/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-04-17_ttt_quali_4/": {
		"title": "TTT Quali 4",
		"description": "",
		"content": "Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 17. April 2021, es fehlen noch Meldungen Sonntag, 18. April 2021, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 17. April 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1) Sonntag, 18. April 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1)",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-04-17_ttt_quali_4/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-04-24_bttv-relegationen/": {
		"title": "BTTV-Relegationen",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 24. April 2021, GT-Halle, es fehlen noch Meldungen Sonntag, 25. April 2021, GT-Halle, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 24. April 2021 Zeit unbekannt Ort GT-Halle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 25. April 2021 Zeit unbekannt Ort GT-Halle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) GT-Halle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-04-24_bttv-relegationen/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-05-04_bundesfinale_jtfo__jtfp/": {
		"title": "Bundesfinale JtfO, JtfP",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Dienstag, 4. Mai 2021, Horst-Korber-Sportzentrum, es fehlen noch Meldungen Mittwoch, 5. Mai 2021, Horst-Korber-Sportzentrum, es fehlen noch Meldungen Donnerstag, 6. Mai 2021, Horst-Korber-Sportzentrum, es fehlen noch Meldungen Freitag, 7. Mai 2021, Horst-Korber-Sportzentrum, es fehlen noch Meldungen Samstag, 8. Mai 2021, Horst-Korber-Sportzentrum, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Dienstag, 4. Mai 2021 Zeit unbekannt Ort Horst-Korber-Sportzentrum, Glockenturmstraße 3-5, 14053 Berlin Oberschiedsrichter_in (fehlend: 1) Mittwoch, 5. Mai 2021 Zeit unbekannt Ort Horst-Korber-Sportzentrum, Glockenturmstraße 3-5, 14053 Berlin Oberschiedsrichter_in (fehlend: 1) Donnerstag, 6. Mai 2021 Zeit unbekannt Ort Horst-Korber-Sportzentrum, Glockenturmstraße 3-5, 14053 Berlin Oberschiedsrichter_in (fehlend: 1) Freitag, 7. Mai 2021 Zeit unbekannt Ort Horst-Korber-Sportzentrum, Glockenturmstraße 3-5, 14053 Berlin Oberschiedsrichter_in (fehlend: 1) Samstag, 8. Mai 2021 Zeit unbekannt Ort Horst-Korber-Sportzentrum, Glockenturmstraße 3-5, 14053 Berlin Oberschiedsrichter_in (fehlend: 1) Horst-Korber-Sportzentrum Glockenturmstraße 3-5, 14053 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-05-04_bundesfinale_jtfo__jtfp/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-05-08_bem_jugend_13/": {
		"title": "BEM Jugend 13",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 8. Mai 2021, Spielhalle, es fehlen noch Meldungen Sonntag, 9. Mai 2021, Spielhalle, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 8. Mai 2021 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 9. Mai 2021 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-05-08_bem_jugend_13/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-05-29_mini-meisterschaften_bestenspiele_jugend_11/": {
		"title": "mini-Meisterschaften Bestenspiele Jugend 11",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Es fehlen noch Meldungen. Details siehe unten. Zusammenfassung Samstag, 29. Mai 2021, GT-Halle, es fehlen noch Meldungen Sonntag, 30. Mai 2021, GT-Halle, es fehlen noch Meldungen Organisatorisches Noch keine Informationen vorhanden. Samstag, 29. Mai 2021 Zeit unbekannt Ort GT-Halle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) Sonntag, 30. Mai 2021 Zeit unbekannt Ort GT-Halle, Paul-Heyse-Straße 25, 10407 Berlin Oberschiedsrichter_in (fehlend: 1) GT-Halle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-05-29_mini-meisterschaften_bestenspiele_jugend_11/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-06-05_ttt_finale/": {
		"title": "TTT Finale",
		"description": "",
		"content": "Es fehlen noch Meldungen. Details siehe unten. Organisatorisches Noch keine Informationen vorhanden. Samstag, 5. Juni 2021 Zeit unbekannt Ort unbekannt Oberschiedsrichter_in (fehlend: 1)",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-06-05_ttt_finale/"
	},
	"https://www.tt-schiri.de/termine/tournaments/2021-06-06_pokalfinale/": {
		"title": "Pokalfinale",
		"description": "",
		"content": "Das Turnier wurde abgesagt. Organisatorisches Noch keine Informationen vorhanden. Sonntag, 6. Juni 2021 Zeit unbekannt Ort Spielhalle, Paul-Heyse-Straße 25, 10407 Berlin Spielhalle Paul-Heyse-Straße 25, 10407 Berlin (Details) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/tournaments/2021-06-06_pokalfinale/"
	},
	"https://www.tt-schiri.de/termine/venues/": {
		"title": "Veranstaltungsorte",
		"description": "",
		"content": "Arnold-Zweig-Sporthalle ASP.5-Sporthalle Ballspielhalle Alte Försterei Brauereien, Grundschule an der Marie Clubraum des Berliner TSC Ernst-Reuter-Schule Freizeitforum Marzahn Geschäftsstelle des BTTV GT-Halle Horst-Korber-Sportzentrum Spielhalle Sport Centrum Siemensstadt Sporthalle Gabriele-von-Bülow-Gymnasium Sporthalle Grundschule am Schäfersee Sporthalle Grundschule Karlsgartenstraße Sporthalle Hatzfeldtallee Sporthalle Hermsdorfer Straße",
		"url": "https://www.tt-schiri.de/termine/venues/"
	},
	"https://www.tt-schiri.de/termine/venues/arnold-zweig-sporthalle/": {
		"title": "Arnold-Zweig-Sporthalle",
		"description": "",
		"content": "Adresse Pestalozzistraße 25, 17309 Pasewalk Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/arnold-zweig-sporthalle/"
	},
	"https://www.tt-schiri.de/termine/venues/asp_5-sporthalle/": {
		"title": "ASP.5-Sporthalle",
		"description": "",
		"content": "Adresse Anton-Saefkow Platz 5, 10369 Berlin Die Halle befindet sich im Fennpfuhl Park zwischen der Landsberger Allee und dem Weißenseer Weg. Anbindung an öffentliche Verkehrsmittel: M8 Haltestelle Anton Saefkow Platz (Fussweg ca. 300m) M13/16 Haltestelle Landsberger Allee / Weißenseer Weg (Fußweg ca. 200m) Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/asp_5-sporthalle/"
	},
	"https://www.tt-schiri.de/termine/venues/ballspielhalle_alte_foersterei/": {
		"title": "Ballspielhalle Alte Försterei",
		"description": "",
		"content": "Adresse Hämmerlingstraße 80, 12555 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/ballspielhalle_alte_foersterei/"
	},
	"https://www.tt-schiri.de/termine/venues/brauereien__grundschule_an_der_marie/": {
		"title": "Brauereien, Grundschule an der Marie",
		"description": "",
		"content": "Adresse Winsstraße 50, 10405 Berlin Eingang zur Halle ist von der Winsstraße vorne oben Treppe hoch. Gehbehinderte Personen bitte zum Haupteingang kommen und klingeln, am besten vorher absprechen. Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/brauereien__grundschule_an_der_marie/"
	},
	"https://www.tt-schiri.de/termine/venues/clubraum_des_berliner_tsc/": {
		"title": "Clubraum des Berliner TSC",
		"description": "",
		"content": "Adresse Paul-Heyse-Straße 25, 10407 Berlin Der Clubraum befindet sich in der 1. Etage. Der Zugang ist besser über die Conrad-Blenkle-Straße an der großen Spielhalle. Vor dem Gebäude sind genügend Fahrradständer. Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/clubraum_des_berliner_tsc/"
	},
	"https://www.tt-schiri.de/termine/venues/ernst-reuter-schule/": {
		"title": "Ernst-Reuter-Schule",
		"description": "",
		"content": "Adresse Bernauer Straße 89, 13355 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/ernst-reuter-schule/"
	},
	"https://www.tt-schiri.de/termine/venues/freizeitforum__marzahn/": {
		"title": "Freizeitforum Marzahn",
		"description": "",
		"content": "Adresse Marzahner Promenade 55, 12679 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/freizeitforum__marzahn/"
	},
	"https://www.tt-schiri.de/termine/venues/geschaeftsstelle_des_bttv/": {
		"title": "Geschäftsstelle des BTTV",
		"description": "",
		"content": "Adresse Paul-Heyse-Straße 29, 10407 Berlin Die Geschäftsstelle befindet sich in der 5. Etage. Anfahrt: die Paul-Heyse-Straße bis zum Ende fahren, bevor es zum Velodrom runtergeht ist links ein kleiner Weg (mit Schranke) zum Gebäude der Geschäftsstelle. Fahrradständer sind vorhanden, mit dem Auto bekommt man auch einen Parkplatz, kann sein, dass man da etwas laufen muss. Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/geschaeftsstelle_des_bttv/"
	},
	"https://www.tt-schiri.de/termine/venues/gt-halle/": {
		"title": "GT-Halle",
		"description": "",
		"content": "Adresse Paul-Heyse-Straße 25, 10407 Berlin Am großen Feld mit der Laufbahn rechts vorbei ganz hinten rechts die Halle ist die GT-Halle. Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/gt-halle/"
	},
	"https://www.tt-schiri.de/termine/venues/horst-korber-sportzentrum/": {
		"title": "Horst-Korber-Sportzentrum",
		"description": "",
		"content": "Adresse Glockenturmstraße 3-5, 14053 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/horst-korber-sportzentrum/"
	},
	"https://www.tt-schiri.de/termine/venues/spielhalle/": {
		"title": "Spielhalle",
		"description": "",
		"content": "Adresse Paul-Heyse-Straße 25, 10407 Berlin Am Eingang des Sportkomplexes am Pförtner vorbei geradeaus durch den engen Gang und hinten ist es dann die große Halle. Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/spielhalle/"
	},
	"https://www.tt-schiri.de/termine/venues/sport_centrum_siemensstadt/": {
		"title": "Sport Centrum Siemensstadt",
		"description": "",
		"content": "Adresse Buolstraße 14, 13629 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/sport_centrum_siemensstadt/"
	},
	"https://www.tt-schiri.de/termine/venues/sporthalle_gabriele-von-buelow-gymnasium/": {
		"title": "Sporthalle Gabriele-von-Bülow-Gymnasium",
		"description": "",
		"content": "Adresse Tile-Brügge-Weg 63, 13509 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/sporthalle_gabriele-von-buelow-gymnasium/"
	},
	"https://www.tt-schiri.de/termine/venues/sporthalle_grundschule_am_schaefersee/": {
		"title": "Sporthalle Grundschule am Schäfersee",
		"description": "",
		"content": "Adresse Baseler Straße 2-6, 13407 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/sporthalle_grundschule_am_schaefersee/"
	},
	"https://www.tt-schiri.de/termine/venues/sporthalle_grundschule_karlsgartenstrasse/": {
		"title": "Sporthalle Grundschule Karlsgartenstraße",
		"description": "",
		"content": "Adresse Karlsgartenstraße 8, 12049 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/sporthalle_grundschule_karlsgartenstrasse/"
	},
	"https://www.tt-schiri.de/termine/venues/sporthalle_hatzfeldtallee/": {
		"title": "Sporthalle Hatzfeldtallee",
		"description": "",
		"content": "Adresse Hatzfeldtallee 29, 13509 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/sporthalle_hatzfeldtallee/"
	},
	"https://www.tt-schiri.de/termine/venues/sporthalle_hermsdorfer_strasse/": {
		"title": "Sporthalle Hermsdorfer Straße",
		"description": "",
		"content": "Adresse Hermsdorfer Straße 27, 12627 Berlin Direkt bei Openstreetmap öffnen",
		"url": "https://www.tt-schiri.de/termine/venues/sporthalle_hermsdorfer_strasse/"
	},
	"https://www.tt-schiri.de/vsr-treffen/": {
		"title": "VSR-Treffen",
		"description": "",
		"content": "Seit 1983 werden in jedem Jahr die Schiedsrichtertreffen der Norddeutschen Verbände und des WTTV durchgeführt. Ausrichter Pokal Laterne 36. 21.06. – 23.06.2019 Cottbus TTVB TTV Brandenburg I TTV Schleswig-Holstein 3 35. 10.08. – 12.08.2018 Bochum WTTV TTV Brandenburg 1 Westdeutscher TT-Verband 3 34. 23.06. – 25.06.2017 Berlin BeTTV TTV Brandenburg I TTV Schleswig-Holstein/Berlin 33. 17.06. – 19.06.2016 Hannover TTVN 32. 05.06. – 07.06.2015 Bremen FTTB 31. 13.06. – 15.06.2014 Schwerin TTVMV 30. 28.06. – 30.06.2013 Wernigerode TTVSA 29. 15.06. – 17.06.2012 Hamburg HTTV 28. 26.08. – 28.08.2011 Preetz TTVSH 27. 31.07. – 02.08.2009 KönigsWusterhausen TTVB 26. 18.07. – 20.07.2008 Düsseldorf WTTV 25. 24.08. – 26.08.2007 Berlin BeTTV 24. 18.08. – 20.08.2006 Hannover TTVN 23. 19.08. – 21.08.2005 Schwerin TTVMV 22. 20.08. – 22.08.2004 Wernigerode TTVSA 21. 22.08. – 24.08.2003 Hamburg HTTV 20. 05.07. – 07.07.2002 Mölln TTVSH 19. 22.06. – 24.06.2001 Bremen FTTB 18. 30.06. – 02.07.2000 Cottbus TTVB 17. 18.06. – 20.06.1999 Oberhausen WTTV 16. 28.08. – 30.08.1998 Berlin BeTTV 15. 26.09. – 28.09.1997 Celle TTVN 14. 16.08. – 18.08.1996 Halle/Saale TTVSA 13. 08.09. – 10.09.1995 Rostock TTVMV 12. 11.11. – 13.11.1994 Hamburg HTTV 11. 13.08. – 15.08.1993 Malente TTVSH 10. 14.08. – 16.08.1992 Bremerhaven FTTB 9. 12.07. – 14.07.1991 Duisburg WTTV 8. 22.06. – 24.06.1990 Berlin BeTTV 7. 01.09. – 03.09.1989 Hameln TTVN 6. 02.09. – 04.09.1988 Hamburg HTTV 5. 04.09. – 06.09.1987 Malente TTVSH 4. 29.08. – 31.08.1986 Bremen FTTB 3. 31.08. – 01.09.1985 Duisburg WTTV 2. 31.08. – 02.09.1984 Berlin BeTTV 1. 27.08. – 28.08.1983 Walsrode TTVN",
		"url": "https://www.tt-schiri.de/vsr-treffen/"
	},
	"https://www.tt-schiri.de/vsr-treffen/34/": {
		"title": "34. Treffen in Berlin",
		"description": "",
		"content": "Der Berliner Tisch-Tennis Verband hatte die Ehre und Freude das 34. Schiedsrichter-Treffen der Nordverbände und des WTTV vom 23. bis 25. Juni 2017 auszurichten. Wir konnten unsere Gäste am Freitag begrüßen, am Sonnabend ging es dann in den Kampf um den Wanderpokal. Alle waren hochmotiviert, die Brandenburger wollten den Pokal zum dritten Mal in Folge verteidigen, die Hamburger gerne die rote Laterne des letzten Jahres weiterreichen. In der warmen Halle traten 76 Spielerinnen und Spieler in 16 Mannschaften zu den Spielen an. Trotz der hohen Temperaturen und dem spielreichen Turniermodus wurden die Punkte bis zum Ende zäh verteidigt, Aufschläge wurden diskutiert, es wurde über Beläge gefachsimpelt und auch das eine oder andere persönliche Gespräch gehalten. Unter der Leitung von Alex Ohle, Chris Nohl und Jörg Penzhorn konnte der Zeitplan fast eingehalten werden, einige sehr lange Sätze verschoben das Turnierende etwas nach hinten. Am Ende des Turniers stand fest, dass Brandenburg seinen Titel erfolgreich verteidigen konnte, gefolgt von Sachsen-Anhalt und dem Westdeutschen TTV. Hamburg gab die rote Laterne an die gemischte Mannschaft aus Schleswig-Holstein und Berlin weiter. Die Mannschaften aus Bremen und Mecklenburg-Vorpommern reihten sich dazwischen ein. Der Turnierabend klang im Holiday Inn bei einem Berliner Abendbüffet gemütlich aus. Hier wurde auch die Siegerehrung durchgeführt. Der Brandenburger TTV darf nach dem dreimaligen Sieg in Folge den Pokal behalten und wird uns im nächsten Jahr mit einem neuen Pokal in Bochum überraschen. Am Sonntag konnten sich alle noch auf eine Schifffahrt über die Spree freuen, das Wetter spielte punktgenau mit und so gelang ein sehr schöner, trockener Abschluss eines sehr angenehmen Treffens. Die Organisation des VSR-Treffens lag komplett auf den Schultern von Walter Zickert, der von Peter Wolff und Chris Nohl unterstützt wurde. Die Organisation war äußerst gelungen und verdient höchste Anerkennung und Lob. Ein Dank auch an die vielen Helfer aus Berlin, die uns tatkräftig unterstützt haben. Wir freuen uns bereits auf das nächste Treffen im August 2018 dort, “wo die Sonne verstaubt”. Ergebnisse TTV Brandenburg I TTV Sachsen-Anhalt I Westdeutscher TTV I TTV Mecklenburg-Vorpommern Berliner TTV I TTV Sachsen-Anhalt II Hamburger TTV I TTV Schleswig-Holstein I TTV Brandenburg II FTT Bremen I Berliner TTV II TTV Schleswig-Holstein II FTT Bremen II Hamburger TTV II Westdeutscher TTV II TTV Schleswig-Holstein/Berlin Berichte Bericht beim TTVB Bericht beim BTTV",
		"url": "https://www.tt-schiri.de/vsr-treffen/34/"
	},
	"https://www.tt-schiri.de/vsr-treffen/35/": {
		"title": "35. Treffen in Bochum",
		"description": "",
		"content": "Traditionell ein Mal im Jahr treffen sich die Schiedsrichter/innen der Nordverbände und des WTTV zu ihren Vergleichskämpfen. In diesem Jahr war der WTTV Ausrichter der nunmehr 35. Auflage, so ging es am Freitag, den 10.08.2018 also in den Pott nach Bochum. Insgesamt waren über 70 Kolleginnen und Kollegen der Zunft aus 8 Bundesländern zugegen. Es ging natürlich um den Wettstreit am Tisch, aber daneben auch um den Austausch untereinander. Wie im Vorjahr daheim waren wir mit zwei Teams am Start. Uta Freyer, Toralf Schley, Frank Röbisch, Rainer Hoffmann, Jörg Penzhorn, Martin Becker, Stefan Labitzke, Christian Nohl und Thomas Brunner hießen die Berliner Vertreter. Mit Begleitungen waren wir dreizehn. Am Freitagabend gönnten wir uns die Bochumer Innenstadt zum Essen und auf ein Bier. Nicht zu lange, wollten wir doch am Samstag unsere Schläger schwingen. An 16 Tischen und in 4 Vierergruppen, die gut gemischt aufgeteilt waren, spielten die Teams die Vorrunde, im Nachgang und je nach Gruppenplatzierung die Endrunde. Berlin II schaffte es am Ende auf Platz 12, die Erste unterlag in der Endrunde deutlich gegen Brandenburg I und wurde Zweiter. Herzlichen Glückwunsch an unsere Nachbarn des TTVB, die den Titel zum vierten Mal in Folge gewannen! Der Abend klang im Saal des Hotels mit Siegerehrung, dem Überreichen von Gastgeschenken, einem opulentem Mahl am Buffet und natürlich intensiven Gesprächen, Plaudereien mit den Kolleg/innen aus Hamburg, Schleswig-Hollstein, Bremen, NRW, M-V, Brandenburg und Niedersachsen aus (Sachsen-Anhalt war in diesem Jahr leider nicht vertreten). Man kennt sich überwiegend langjährig, so gab es ein sehr herzliches Miteinander. Für den Sonntag stand ein Besuch des Deutschen Bergbau-Museums Bochum auf dem Programm, im Anschluss wurde zur Stärkung am „Büdchen“ nebenan die „Bergmann-Kombi“ (Currywurst, Pommes & Getränk) kredenzt, bevor alle die Heimreise antraten. Ein ganz herzliches Dankeschön und auch Kompliment an das Org.-Team um Hansi Heinbuch und Willi Klaßen, die Veranstaltung war mit rheinischem Charme sehr liebevoll organisiert und nicht nur wir Berliner/innen haben uns sehr wohl gefühlt. Im kommenden Jahr wird die Reise nicht so weit sein, wenn im Brandenburgischen Cottbus die Vergleichskämpfe ausgetragen werden. Ergebnisse TTV Brandenburg 1 Berliner TTV 1 TTV Schleswig-Holstein 1 Westdeutscher TT-Verband 1 FTT Bremen 1 Hamburger TTV 1 TTV Mecklenburg-Vorpommern 1 Hamburger TTV 2 TTV Brandenburg 2 Westdeutscher TT-Verband 2 TTV Schleswig-Holstein 2 Berliner TTV 2 Mixed-Team FTT Bremen 2 TTV Schleswig-Holstein 3 Westdeutscher TT-Verband 3 Berichte Bericht beim BTTV",
		"url": "https://www.tt-schiri.de/vsr-treffen/35/"
	},
	"https://www.tt-schiri.de/vsr-treffen/36/": {
		"title": "36. Treffen in Cottbus",
		"description": "",
		"content": "Am 22./23. Juni 2019 trafen sich in Cottbus die Mannschaften aus den Nordverbänden Bremen, Hamburg, Schleswig-Holstein, Mecklenburg-Vorpommern, Brandenburg, Nordrhein-Westfalen, Sachsen-Anhalt und Berlin zum jährlichen Stelldichein. Leider waren die Kollegen aus Niedersachsen nicht dabei, das wird im kommenden Jahr hoffentlich wieder anders sein. Der BTTV schickte gleich zwei Teams ins Rennen, die Anreise war ja nicht allzu weit für uns. Die Berliner Delegation um Falk Haferkorn, Frank Röbisch, Christian Nohl, Martin Becker, Stefan Labitzke und Thomas Brunner wurde angeführt von Schiri-Urgestein und Verbands-Ehrenmitglied Walter Zickert, der seit 1967 als Schiedsrichter aktiv ist und viele Jahre Obmann des BTTV war. Eine im Vorfeld bekannt gewordene Absage wurde durch Nicole Löschner (WTTV) glatt kompensiert, vielen Dank dafür 😉 ! Martin nahm die Organisation für uns in seine bewährten Hände, alles klappte somit geräuschlos. Es waren in Summe 15 Mannschaften am Start, gespielt wurde zunächst in 4 Gruppen jeder gegen jeden und über 2 Gewinnsätze. Die Endrunde der Gleichplatzierten Teams aus der Vorrunde wurde dann über 3 Gewinnsätze ausgetragen. Bei dem Turnier geht es nicht vordergründig „um alles“, so wurden einige Teams auf Grund von Absagen mit Spielenden anderer Verbände bereichert. Das Spielsystem – wie soll es anders sein? – steht in keiner Ordnung: 4-er Mannschaft mit zwei frei wählbaren Startdoppeln, dann 1-1, 2-2, 3-3 und 4-4, wobei der TTR/die Live-PZ die Rangfolge innerhalb der Mannschaften vorgibt. Neben gleich 8 Abweichungen zur WO stand in den Durchführungsbestimmungen zum Turnier auch Alkoholische Getränke … sind in der Sporthalle untersagt. Das war eigentlich unnötig, denn in der Spielstätte Sandow waren am Samstag gefühlte 40 °C. Und gekühlte Getränke mit wurden einvernehmlich und ohne Protest am Freitag- und Samstagabend genossen. Das Radisson Blue bot den Teilnehmenden auch dafür beste Bedingungen. Unsere Zweite mit Nicole, Falk, Frank und Walter beendete als Zweiter die Gruppe nach Niederlagen gegen TTVSH-1 und HaTTV-2 sowie einem Sieg gegen WTTV/TTVSA. In der Endrunde um die Plätze 5-8 dann blieb man sogar ungeschlagen und landete nach Unentschieden gegen TTVSH/TTVMV und HaTTV-1 und einem Sieg gegen TTVSH-2 auf dem sechsten Platz. Das war nicht zu erwarten und höchst erfreulich für uns, die Sorge um die Rote Laterne völlig unbegründet und nur von ganz kurzer Dauer! Unsere Erste zählte auch in diesem Jahr ein bisschen zum Favoritenkreis, wobei ja nie ganz klar ist, wen die Verbände alles werden aufbieten können. Die Vorrunde überstanden wir makellos gegen TTVB-2, TTVSH-3 und TTVSH/TTVMV. In der Endrunde glänzten wir abermals gegen TTVSH-1 und TTVSA, es kam also zum erhofften Endspiel gegen Dauerrivale TTV Brandenburg-1. Da wir bis hier 2 Sätze mehr abgaben als die Brandenburger und die Aufstellungen beider Mannschaften sehr ausgewogen waren, konnte uns nur ein Sieg zum Gesamterfolg helfen. Der Anfang ließ aufhorchen, wir brachten die Gastgeber mit einer 3:1-Führung in arge Bedrängnis. Christian führte gar noch 2:1 gegen Altmeister Bela Balint, am Ende jedoch unterlagen er und auch Martin, das Unentschieden reichte somit nicht zum Turniersieg, 3 Sätze fehlten uns. Die Enttäuschung hielt sich in Grenzen, das reich und bunt gedeckte Buffet und die unzähligen Gespräche mit den Kolleginnen und Kollegen der anderen Verbände am Abend taten ihr Übriges. Von Verbandsschiedsrichtern bis hin zu International Umpire-Blue Badge, den Schiris am Tisch mit der weltweit höchsten Qualifikation, war alles vertreten. Man traf alte und neue Bekannte, hatte viel zu erzählen über Vereins- und Verbandsarbeit, Anekdoten aus dem Schirileben, von gemeinsamen Erfahrungen. Nach der Siegerehrung am Abend wurde unser Stefan mit dem Pokal der Stadt Cottbus zudem als Bester Einzelspieler des Turniers geehrt, und das zu Recht! Das Ausrichter-Team um Steffen Swat und Giselher Segieth wurde von den teilnehmenden Verbänden mit Souvenirs und lukullischen Köstlichkeiten dankend beschenkt, bevor der Cottbuser Postkutscher auf humoristische Art Wissen über die Region der Austragungsstadt zum Besten gab. Die Gastgeber boten am Sonntag zum Abschluss eine schöne Kahnfahrt im nahen Spreewald an, die wir und unsere Begleitungen gerne nutzten. Vielen Dank und unser Kompliment an die Brandenburger! Alles in allem erlebten wir wieder eine sehr gelungene Veranstaltung, die nach Fortsetzung ruft: im kommenden Jahr werden die Kollegen aus Bremen Gastgeber sein – das sollten wir uns nicht entgehen lassen! Ergebnisse TTV Brandenburg I Berliner TTV 1 TTV Schleswig-Holstein 1 TTV Sachsen-Anhalt TTV Mecklenburg-Vorpommern Berliner TTV 2 Hamburger TTV 1 TTV Schleswig-Holstein 2 TTV Brandenburg 2 FTT Bremen 2 TTV Brandenburg 3 Westdeutscher TTV FTT Bremen 1 Hamburger TTV 2 TTV Schleswig-Holstein 3 Berichte Bericht beim BTTV",
		"url": "https://www.tt-schiri.de/vsr-treffen/36/"
	}
};